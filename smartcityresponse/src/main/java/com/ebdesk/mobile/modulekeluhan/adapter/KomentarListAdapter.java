package com.ebdesk.mobile.modulekeluhan.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ebdesk.mobile.modulekeluhan.R;
import com.ebdesk.mobile.modulekeluhan.databinding.ItemKomentarBinding;
import com.ebdesk.mobile.modulekeluhan.model.Komentar;
import com.ebdesk.mobile.modulekeluhan.viewmodel.viewmodelitem.ItemKomentarViewModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by EB-NB21 on 3/3/2017.
 */

public class KomentarListAdapter extends RecyclerView.Adapter<KomentarListAdapter.KomentarViewHolder>{

    private List<Komentar> listKomentar;

    public KomentarListAdapter() {
        this.listKomentar = Collections.emptyList();
    }

    public KomentarListAdapter(List<Komentar> listKomentar) {
        this.listKomentar = listKomentar;
    }

    public void setListKomentar(List<Komentar> listKomentar) {
        this.listKomentar = listKomentar;
    }
    @Override
    public KomentarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemKomentarBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_komentar,
                parent,
                false);
        return new KomentarViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(KomentarViewHolder holder, int position) {
        holder.bindKomentar(listKomentar.get(position));
    }

    @Override
    public int getItemCount() {
        return listKomentar.size();
    }

    public class KomentarViewHolder extends RecyclerView.ViewHolder{

        final ItemKomentarBinding binding;

        public KomentarViewHolder(ItemKomentarBinding binding) {
            super(binding.llayoutKomentar);
            this.binding = binding;
        }

        void bindKomentar(Komentar komentar) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemKomentarViewModel(itemView.getContext(), komentar));
            } else {
                binding.getViewModel().setKomentar(komentar);
            }
        }
    }
}
