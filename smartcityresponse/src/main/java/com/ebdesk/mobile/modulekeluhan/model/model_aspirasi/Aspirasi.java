
package com.ebdesk.mobile.modulekeluhan.model.model_aspirasi;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Aspirasi implements Parcelable
{

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<AspirasiData> data = null;
    public final static Creator<Aspirasi> CREATOR = new Creator<Aspirasi>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Aspirasi createFromParcel(Parcel in) {
            Aspirasi instance = new Aspirasi();
            instance.status = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.data, (AspirasiData.class.getClassLoader()));
            return instance;
        }

        public Aspirasi[] newArray(int size) {
            return (new Aspirasi[size]);
        }

    }
    ;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Aspirasi() {
    }

    /**
     * 
     * @param message
     * @param status
     * @param data
     */
    public Aspirasi(Boolean status, String message, List<AspirasiData> data) {
        super();
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AspirasiData> getData() {
        return data;
    }

    public void setData(List<AspirasiData> data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeList(data);
    }

    public int describeContents() {
        return  0;
    }

}
