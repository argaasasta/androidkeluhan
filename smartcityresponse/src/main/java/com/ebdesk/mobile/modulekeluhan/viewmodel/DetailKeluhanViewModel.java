package com.ebdesk.mobile.modulekeluhan.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import com.ebdesk.mobile.modulekeluhan.KeluhanApplication;
import com.ebdesk.mobile.modulekeluhan.adapter.KeluhanListAdapter;
import com.ebdesk.mobile.modulekeluhan.databinding.FragmentDetailKeluhanBinding;
import com.ebdesk.mobile.modulekeluhan.model.Komentar;
import com.ebdesk.mobile.modulekeluhan.model.ServiceApi;
import com.ebdesk.mobile.modulekeluhan.model.model_aspirasi.Aspirasi;
import com.ebdesk.mobile.modulekeluhan.model.model_aspirasi.AspirasiData;

import java.util.ArrayList;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by EB-NB21 on 3/1/2017.
 */

public class DetailKeluhanViewModel extends BaseObservable implements ViewModel {
    private static final String TAG = DetailKeluhanViewModel.class.getSimpleName();

    private AspirasiData aspirasiData;
    private Context context;
    private Subscription subscription;
    private DetailKeluhanListener detailKeluhanListener;
    public ObservableArrayList<Komentar> listKomentar;
    public ObservableArrayList<AspirasiData> listAspirasiData;
    FragmentDetailKeluhanBinding binding;
    public String komentarString;
    public ObservableField<String> komentarStringObs;
    public ObservableInt pictureVisibility;
    public ObservableInt videoVisibility;

    public DetailKeluhanViewModel(Context context, final AspirasiData aspirasiData, DetailKeluhanListener detailKeluhanListener, FragmentDetailKeluhanBinding binding) {
        this.aspirasiData = aspirasiData;
        this.context = context;
        this.detailKeluhanListener = detailKeluhanListener;
        listKomentar = new ObservableArrayList<>();
        listAspirasiData = new ObservableArrayList<>();
        this.komentarStringObs = new ObservableField<>();
        this.binding = binding;

        pictureVisibility = new ObservableInt(View.VISIBLE);
        videoVisibility = new ObservableInt(View.VISIBLE);

        if(aspirasiData.getFilePath().contains(".mp4")){
            pictureVisibility.set(View.GONE);
            videoVisibility.set(View.VISIBLE);
        }else{
            pictureVisibility.set(View.VISIBLE);
            videoVisibility.set(View.GONE);
        }
        initDummyListKomentar();
        initAspirasiTerkait();
    }

    public int visibility(){
        aspirasiData.getFilePath().equals("");
        return View.VISIBLE;
    }

    public String getId() {
        return aspirasiData.getIdAspirasi();
    }

    public String getCreatorName() {
        return aspirasiData.getCreatedBy();
    }

    public String getCreatorProfilePicture() {
        return aspirasiData.getFilePath();
    }

    public String getCategory() {
        return aspirasiData.getCategoryName();
    }

    public int getStatus() {
        return 1;
    }

    public String getStringStatus() {
        String status = "Waiting";
//        if (aspirasiData.getStatusProgress() == 1) {
//            status="Waiting";
//        } else if (aspirasiData.getStatusProgress() == 2) {
//            status="In Progress";
//        } else if (aspirasiData.getStatusProgress() == 3) {
//            status="Done";
//        }
        return status;
    }

    public String getWatchers() {
        return "" + 20;
    }

    public String getLikers() {
        return "" + 21;
    }

    public String getCommenters() {
        return "" + 21;
    }

    public String getKeluhanImage() {
        return aspirasiData.getFilePath();
    }

    public String getLocationDesc() {
        return aspirasiData.getLocationDesc();
    }

    public String getLocationLatitude() {
        return aspirasiData.getLatitude();
    }

    public String getLocationLongitude() {
        return aspirasiData.getLongitude();
    }

    public String getDate() {
        return aspirasiData.getCreatedTime();
    }

    public String getTitle() {
        return aspirasiData.getDesc();
    }

    public String getDesc() {
        return aspirasiData.getDesc();
    }

    public void onBtnCommentClick(View view){
        detailKeluhanListener.onBtnCommentClicked();
    }

    public interface DetailKeluhanListener {
        void onBtnCommentClicked();
    }

    public TextWatcher getCommentEditTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                komentarString = charSequence.toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
    }
    @BindingAdapter({"loadList"})
    public static void loadListRecycle(RecyclerView view, ObservableArrayList<AspirasiData> listAspirasiData) {
        KeluhanListAdapter adapter =
                (KeluhanListAdapter) view.getAdapter();
        adapter.setListAspirasiData(listAspirasiData);
        adapter.notifyDataSetChanged();
    }

    public void initAspirasiTerkait() {
        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        KeluhanApplication application = KeluhanApplication.get(context);

        application.setServiceApi(ServiceApi.Factory.createServiceInfoKota());
        ServiceApi serviceApi = application.getServiceApi();
        System.out.println("ASPIRASI TERKAIT DENGAN CAT "+getCategory()+" dan "+listAspirasiData.size());
        subscription = serviceApi.publicGetAspirasiTerkait(getCategory(),listAspirasiData.size())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(new Subscriber<Aspirasi>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println("ADA ERROR ASPIRASI TERKAIT");
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Aspirasi aspirasi) {
                        for (int i = 0; i < aspirasi.getData().size(); i++) {
                            System.out.println("ISI API ASPIRASI TERKAIT" + aspirasi.getData().get(i).getDesc());
                            listAspirasiData.add(aspirasi.getData().get(i));
                        }
                    }
                });
        notifyChange();
    }

    public void onClickSend(View view){
        Toast.makeText(context, "ISI "+komentarString, Toast.LENGTH_SHORT).show();
//        Toast.makeText(context, "ISI "+komentarString, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void destroy() {
        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        subscription = null;
        context = null;
        detailKeluhanListener = null;
    }

    private void initDummyListKomentar(){
        for (int i = 0; i < 10; i++) {
            listKomentar.add(new Komentar("00"+i,"name"+i,"https://www.w3schools.com/css/img_fjords.jpg","Komentar "+i,"https://www.w3schools.com/css/img_fjords.jpg","12-10-2017",i%3));
        }
    }


}
