package com.ebdesk.mobile.modulekeluhan.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebdesk.mobile.modulekeluhan.R;

import com.ebdesk.mobile.modulekeluhan.databinding.ItemInfokotaBinding;
import com.ebdesk.mobile.modulekeluhan.model.model_info_kota.ItemInfoKota;
import com.ebdesk.mobile.modulekeluhan.viewmodel.viewmodelitem.ItemInfoKotaViewModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by EB-NB21 on 3/10/2017.
 */

public class InfoKotaListAdapter extends RecyclerView.Adapter<InfoKotaListAdapter.InfoKotaViewHolder> {

    private List<ItemInfoKota> listItemInfoKota;

    public InfoKotaListAdapter() {
        this.listItemInfoKota = Collections.emptyList();
    }

    public InfoKotaListAdapter(List<ItemInfoKota> listItemInfoKota) {
        this.listItemInfoKota = listItemInfoKota;
    }

    public void setListInfoKota(List<ItemInfoKota> listItemInfoKota) {
        this.listItemInfoKota = listItemInfoKota;
        notifyDataSetChanged();
    }

    @Override
    public InfoKotaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemInfokotaBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_infokota,
                parent,
                false);
        binding.llItemKota.setCardBackgroundColor(parent.getContext().getResources().getColor(R.color.white_opaque));
        binding.rivDescImage.setVisibility(View.GONE);
        return new InfoKotaViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(InfoKotaViewHolder holder, int position) {
        holder.bindInfoKota(listItemInfoKota.get(position));
    }

    @Override
    public int getItemCount() {
        return listItemInfoKota.size();
    }

    public class InfoKotaViewHolder extends RecyclerView.ViewHolder {

        final ItemInfokotaBinding binding;

        public InfoKotaViewHolder(ItemInfokotaBinding binding) {
            super(binding.llItemKota);
            this.binding = binding;
        }

        void bindInfoKota(ItemInfoKota itemInfoKota) {
//            if(itemInfoKota.getInfoImage().equals("")){
//
//                binding.rivDescImage.setVisibility(View.GONE);
//            }else{
//                binding.rivDescImage.setVisibility(View.VISIBLE);
//            }
            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemInfoKotaViewModel(itemView.getContext(), itemInfoKota));
            } else {
                binding.getViewModel().setInfoKotaOld(itemInfoKota);
            }
        }
    }
}
