package com.ebdesk.mobile.modulekeluhan.view.dialog;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.ebdesk.mobile.modulekeluhan.R;
import com.ebdesk.mobile.modulekeluhan.model.model_category.CategoryData;

import java.util.List;

/**
 * Created by EB-NB21 on 11/15/2016.
 */

public class ListCategoryAdapter extends BaseAdapter {

    private List<CategoryData> categoryList;
    private Context mContext;
    public ListCategoryAdapter(Context context, List<CategoryData> categoryList) {
        this.mContext=context;
        this.categoryList=categoryList;
    }

    @Override
    public int getCount() {
        return categoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return categoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_category, parent, false);

        TextView tvCategoryName = (TextView) rowView.findViewById(R.id.tv_category_name);
        tvCategoryName.setText(categoryList.get(position).getName());

        String imageUrl = "http://103.43.128.142:8880/"+categoryList.get(position).getPathPhoto();
        ImageView imageViewCategory = (ImageView) rowView.findViewById(R.id.iv_category_image);
        Glide.with(mContext)
                .load(imageUrl).asBitmap().centerCrop()
                .override(100,100)
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .into(new BitmapImageViewTarget(imageViewCategory) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(view.getContext().getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        view.setImageDrawable(circularBitmapDrawable);
                    }
                });
        return rowView;
    }


}
