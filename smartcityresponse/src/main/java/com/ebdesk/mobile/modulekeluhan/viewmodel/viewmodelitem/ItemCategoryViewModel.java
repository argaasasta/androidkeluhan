package com.ebdesk.mobile.modulekeluhan.viewmodel.viewmodelitem;

import android.databinding.BaseObservable;
import android.view.View;

import com.ebdesk.mobile.modulekeluhan.model.model_category.CategoryData;
import com.ebdesk.mobile.modulekeluhan.viewmodel.ViewModel;

/**
 * Created by EB-NB21 on 3/29/2017.
 */

public class ItemCategoryViewModel extends BaseObservable implements ViewModel {

    private CategoryData categoryData;
    private CategorySelectedListener categorySelectedListener;

    public interface CategorySelectedListener{
        public void onCategorySelected(CategoryData categoryData);
    }
    public ItemCategoryViewModel(CategoryData categoryData,CategorySelectedListener categorySelectedListener) {
        this.categoryData = categoryData;
        this.categorySelectedListener=categorySelectedListener;
    }

    public void setCategory(CategoryData categoryData) {
        this.categoryData = categoryData;
        notifyChange();
    }

    public String getCategoryName(){
        return categoryData.getName();
    }

    public String getCategoryImage(){
        return categoryData.getPathPhoto();
    }

    public void onCategoryClick(View view){
        categorySelectedListener.onCategorySelected(categoryData);
    }

    @Override
    public void destroy() {

    }
}
