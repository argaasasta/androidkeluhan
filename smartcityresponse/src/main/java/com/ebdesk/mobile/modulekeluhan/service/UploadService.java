package com.ebdesk.mobile.modulekeluhan.service;

import android.app.IntentService;
import android.content.Intent;
import android.media.ExifInterface;
import android.support.annotation.Nullable;
import android.view.View;

import com.ebdesk.mobile.modulekeluhan.KeluhanApplication;
import com.ebdesk.mobile.modulekeluhan.model.ServiceApi;
import com.ebdesk.mobile.modulekeluhan.util.video_converter.VideoResolutionChanger;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by EB-NB21 on 3/31/2017.
 */

public class UploadService extends IntentService{
    private Subscription subscription;

    public UploadService() {
        super("UploadService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        KeluhanApplication application = KeluhanApplication.get(getApplicationContext());

        application.setServiceApi(ServiceApi.Factory.createServiceAspirasi(ServiceApi.TYPE_STRING));
        ServiceApi serviceApi = application.getServiceApi();

        String categoryIdSelected =  intent.getStringExtra("CATEGORY_ID");
        String categoryString =  intent.getStringExtra("CATEGORY_NAME");
        String descString =  intent.getStringExtra("DESCRIPTION");
        String latitudeString =  intent.getStringExtra("LATITUDE");
        String longitudeString =  intent.getStringExtra("LONGITUDE");
        String creatorName =  intent.getStringExtra("CREATED_BY");
        String type =  intent.getStringExtra("MEDIA_TYPE");
        String mediaPath =  intent.getStringExtra("VIDEO_PATH");

        File rawFile = new File(mediaPath);
        File videoFile = null;
        System.out.println("Size video first" + rawFile.length());

        //if(rawFile.length()>8000000) {
        try {
            String pathToReEncodedFile =
                    new VideoResolutionChanger().changeResolution(rawFile);
            videoFile = new File(pathToReEncodedFile);
            System.out.println("Size video after compress" + videoFile.length());
        } catch (Throwable throwable) {
            System.out.println("CANNOT COMPRESS");
            throwable.printStackTrace();
        }

        RequestBody categoryId = RequestBody.create(MediaType.parse("text/plain"), categoryIdSelected);
        RequestBody categoryName = RequestBody.create(MediaType.parse("text/plain"), categoryString);
        RequestBody desc = RequestBody.create(MediaType.parse("text/plain"), descString);
        RequestBody latitude = RequestBody.create(MediaType.parse("text/plain"),latitudeString);
        RequestBody longitude = RequestBody.create(MediaType.parse("text/plain"), longitudeString);
        RequestBody createdBy = RequestBody.create(MediaType.parse("text/plain"), creatorName);
        RequestBody mediaType = RequestBody.create(MediaType.parse("text/plain"), type);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("photo", videoFile.getName(), RequestBody.create(MediaType.parse("image/*"), videoFile));
        subscription = serviceApi.postUpdateActivity(categoryId,categoryName,desc,latitude,longitude,createdBy,mediaType,filePart)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(new Subscriber<String>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println("ERROR SEND KELUHAN VIDEO");
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(String response) {
                        System.out.println("RESPONSE " + response);
                    }

                });
    }
}
