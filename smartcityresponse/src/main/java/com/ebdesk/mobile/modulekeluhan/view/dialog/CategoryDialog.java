package com.ebdesk.mobile.modulekeluhan.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;

import com.ebdesk.mobile.modulekeluhan.R;
import com.ebdesk.mobile.modulekeluhan.model.model_category.CategoryData;

import java.util.List;

/**
 * Created by EB-NB21 on 11/15/2016.
 */

public class CategoryDialog extends Dialog {

    private List<CategoryData> categoryList;
    private ListCategoryAdapter listCategoryAdapter;
    private Context mContext;

    public interface OnPickCategoryListener{
        public void onCategorySelected(CategoryData categoryData);
    }

    private OnPickCategoryListener onPickCategoryListener;
    public CategoryDialog(Context context, List<CategoryData> categoryList, OnPickCategoryListener onPickCategoryListener) {
        super(context, R.style.full_screen_dialog);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        this.mContext=context;
        this.categoryList=categoryList;
        this.onPickCategoryListener = onPickCategoryListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_pick_category);


        GridView gridview = (GridView) findViewById(R.id.gridview_category);
        gridview.setAdapter(new ListCategoryAdapter(mContext,categoryList));
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
//                Toast.makeText(mContext, "" + position,Toast.LENGTH_SHORT).show();
                onPickCategoryListener.onCategorySelected(categoryList.get(position));
                dismiss();
            }
        });


    }
}
