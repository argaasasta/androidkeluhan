package com.ebdesk.mobile.modulekeluhan.view;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebdesk.mobile.modulekeluhan.R;
import com.ebdesk.mobile.modulekeluhan.adapter.InfoKotaListAdapter;
import com.ebdesk.mobile.modulekeluhan.databinding.FragmentInfoKotaBinding;
import com.ebdesk.mobile.modulekeluhan.util.EndlessRecyclerViewScrollListener;
import com.ebdesk.mobile.modulekeluhan.viewmodel.InfoKotaViewModel;

public class InfoKotaFragment extends Fragment implements InfoKotaViewModel.OnRefreshInfoKota{
    private final String KEY_RECYCLER_STATE = "recycler_state";
    FragmentInfoKotaBinding binding;
    InfoKotaViewModel infoKotaViewModel;
    Bundle mBundleRecyclerViewState;
    public static InfoKotaFragment newInstance() {
        InfoKotaFragment fragment = new InfoKotaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_info_kota, container, false);
        View view = binding.getRoot();
        infoKotaViewModel = new InfoKotaViewModel(getContext(),this);
        setupRecyclerView(binding.rvInfoKotaList);
        binding.setViewModel(infoKotaViewModel);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        infoKotaViewModel.destroy();
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        InfoKotaListAdapter adapter = new  InfoKotaListAdapter();
        recyclerView.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        EndlessRecyclerViewScrollListener scrollListener;
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                System.out.println("BERAPA SIH TOTAL ITEMNYA "+totalItemsCount);
                infoKotaViewModel.loadInfoKota(totalItemsCount,5);
            }
        };
        recyclerView.addOnScrollListener(scrollListener);
        recyclerView.setItemViewCacheSize(10000);
        recyclerView.setHasFixedSize(true);
        binding.swipeRefreshLayoutInfoKota.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                infoKotaViewModel.initInfoKotaFirst();
            }
        });
    }

    @Override
    public void onItemsLoadComplete() {
        binding.swipeRefreshLayoutInfoKota.setRefreshing(false);
    }
}
