package com.ebdesk.mobile.modulekeluhan.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebdesk.mobile.modulekeluhan.R;
import com.ebdesk.mobile.modulekeluhan.databinding.ItemKeluhanBinding;
import com.ebdesk.mobile.modulekeluhan.model.model_aspirasi.AspirasiData;
import com.ebdesk.mobile.modulekeluhan.viewmodel.AspirasiKeluhanViewModel;
import com.ebdesk.mobile.modulekeluhan.viewmodel.viewmodelitem.ItemKeluhanViewModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by EB-NB21 on 3/1/2017.
 */

public class KeluhanListAdapter extends RecyclerView.Adapter<KeluhanListAdapter.KeluhanViewHolder> {

    private Context context;
    private List<AspirasiData> listAspirasiData;
    private AspirasiKeluhanViewModel aspirasiKeluhanViewModel;
    AdapterKeluhanListener adapterKeluhanListener;
    private String type;
    public KeluhanListAdapter(AdapterKeluhanListener adapterKeluhanListener,String type) {
        this.adapterKeluhanListener = adapterKeluhanListener;
        this.listAspirasiData = Collections.emptyList();
        this.type = type;
    }

    public KeluhanListAdapter(List<AspirasiData> listAspirasiData) {
        this.listAspirasiData = listAspirasiData;
    }

    public void setListAspirasiData(List<AspirasiData> listAspirasiData) {
        this.listAspirasiData = listAspirasiData;

    }

    @Override
    public KeluhanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemKeluhanBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_keluhan, parent, false);
        context = parent.getContext();
        return new KeluhanViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(KeluhanViewHolder holder, final int position) {
        holder.bindKeluhan(listAspirasiData.get(position));
        if(type.equals("aspirasi")) {
            holder.binding.cardViewKeluhan.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    aspirasiKeluhanViewModel.positionSelected.set(position);
                    adapterKeluhanListener.onLongClickKeluhan(position);
                    return true;
                }
            });
        }
    }

    public void setAspirasiViewModel(AspirasiKeluhanViewModel aspirasiViewModel) {
        this.aspirasiKeluhanViewModel = aspirasiViewModel;
    }

    @Override
    public int getItemCount() {
        return listAspirasiData.size();
    }

    public class KeluhanViewHolder extends RecyclerView.ViewHolder {

        final ItemKeluhanBinding binding;

        public KeluhanViewHolder(ItemKeluhanBinding binding) {
            super(binding.cardViewKeluhan);
            this.binding = binding;
        }

        void bindKeluhan(AspirasiData aspirasiData) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemKeluhanViewModel(itemView.getContext(), aspirasiData));
            } else {
                binding.getViewModel().setAspirasiData(aspirasiData);
            }
        }
    }

    public interface AdapterKeluhanListener {
        public void onLongClickKeluhan(int position);
    }
}
