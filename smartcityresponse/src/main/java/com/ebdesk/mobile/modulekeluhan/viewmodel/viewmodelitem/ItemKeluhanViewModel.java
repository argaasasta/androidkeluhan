package com.ebdesk.mobile.modulekeluhan.viewmodel.viewmodelitem;

import android.content.Context;
import android.databinding.BaseObservable;
import android.view.View;

import com.ebdesk.mobile.modulekeluhan.model.model_aspirasi.AspirasiData;
import com.ebdesk.mobile.modulekeluhan.view.DetailKeluhanActivity;
import com.ebdesk.mobile.modulekeluhan.viewmodel.ViewModel;

/**
 * Created by EB-NB21 on 3/1/2017.
 */

public class ItemKeluhanViewModel extends BaseObservable implements ViewModel {

    private AspirasiData aspirasiData;
    private Context context;

    public ItemKeluhanViewModel(Context context, AspirasiData aspirasiData) {
        this.aspirasiData = aspirasiData;
        this.context = context;
    }

    public String getId() {
        return aspirasiData.getIdAspirasi();
    }

    public String getCreatorName() {
        return aspirasiData.getCreatedBy();
    }

    public String getCategory() {
        return aspirasiData.getCategoryName();
    }

    public int getStatus() {
//        System.out.println("STATUSNYA APA SIH "+aspirasiData.getStatusProgress());
        return 1;
    }

    public String getWatchers() {
        return "" + 20;
    }

    public String getLikers() {
        return "" + 21;
    }

    public String getCommenters() {
        return "" + 10;
    }

    public String getKeluhanImage() {
        if (aspirasiData.getThumbPath() != null) {
            if (aspirasiData.getThumbPath().equals("")) {
                return aspirasiData.getFilePath();
            } else {
                return "http://103.43.128.142:8880/"+aspirasiData.getThumbPath();
            }
        } else {
            return "";
        }


    }

    public String getLocationDesc() {
        return aspirasiData.getLocationAdmArea3() + ", " + aspirasiData.getLocationAdmArea2();
    }

    public String getDate() {
        return aspirasiData.getCreatedTime();
    }

    public String getTitle() {
        return aspirasiData.getDesc();
    }

    public String getDesc() {
        return aspirasiData.getDesc();
    }

    public void onItemClick(View view) {
        context.startActivity(DetailKeluhanActivity.newIntent(context, aspirasiData));
    }

    // Allows recycling ItemRepoViewModels within the recyclerview adapter
    public void setAspirasiData(AspirasiData aspirasiData) {
        this.aspirasiData = aspirasiData;
        notifyChange();
    }

    @Override
    public void destroy() {
        //In this case destroy doesn't need to do anything because there is not async calls
    }

}
