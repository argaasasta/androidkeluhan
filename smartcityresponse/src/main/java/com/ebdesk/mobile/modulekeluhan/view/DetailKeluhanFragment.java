package com.ebdesk.mobile.modulekeluhan.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ebdesk.mobile.modulekeluhan.R;
import com.ebdesk.mobile.modulekeluhan.adapter.KeluhanListAdapter;
import com.ebdesk.mobile.modulekeluhan.adapter.KomentarListAdapter;

import com.ebdesk.mobile.modulekeluhan.databinding.FragmentDetailKeluhanBinding;
import com.ebdesk.mobile.modulekeluhan.model.model_aspirasi.AspirasiData;
import com.ebdesk.mobile.modulekeluhan.util.ui.CustomVideoView;
import com.ebdesk.mobile.modulekeluhan.viewmodel.DetailKeluhanViewModel;

/**
 * Created by EB-NB21 on 3/31/2017.
 */

public class DetailKeluhanFragment extends Fragment implements DetailKeluhanViewModel.DetailKeluhanListener, KeluhanListAdapter.AdapterKeluhanListener  {

    private static final String EXTRA_KELUHAN = "EXTRA_KELUHAN";
    private FragmentDetailKeluhanBinding binding;
    private DetailKeluhanViewModel detailKeluhanViewModel;

    private BottomSheetBehavior<View> mBottomSheetBehavior;

    public static Intent newIntent(Context context, AspirasiData aspirasiData) {
        Intent intent = new Intent(context, DetailKeluhanActivity.class);
        intent.putExtra(EXTRA_KELUHAN, aspirasiData);
        return intent;
    }

    public static DetailKeluhanFragment newInstance(AspirasiData aspirasiData) {
        DetailKeluhanFragment fragment = new DetailKeluhanFragment();
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_KELUHAN,aspirasiData);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static DetailKeluhanFragment newInstance(Context context,Intent intent) {
        AspirasiData aspirasiData = intent.getParcelableExtra(context.getString(R.string.key_extra_keluhan));//ga boleh nih
        DetailKeluhanFragment fragment = new DetailKeluhanFragment();
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_KELUHAN,aspirasiData);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            // Respond to the action bar's Up/Home button
//            case android.R.id.home:
//                finish();
//                return true;
//
//        }
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail_keluhan, container, false);
        View view = binding.getRoot();

//        ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.setDisplayHomeAsUpEnabled(true);
//        }
        setupRecyclerView(binding.commentLayout.rvCommentList);
        setupRecyclerViewTerkait(binding.rvKeluhanList);

        AspirasiData aspirasiData = getArguments().getParcelable(EXTRA_KELUHAN);
        detailKeluhanViewModel = new DetailKeluhanViewModel(getActivity(), aspirasiData, this, binding);

        mBottomSheetBehavior = BottomSheetBehavior.from((View) binding.commentLayout.layoutCommentActivity);
        mBottomSheetBehavior.setPeekHeight(0);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {

                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        //Currently there is no way of setting an activity title using data binding
//        setTitle(aspirasiData.getDesc());
        binding.scrollViewDetail.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                View view = (View) v.getChildAt(v.getChildCount() - 1);
                int diff = (view.getBottom() - (v.getHeight() + v.getScrollY()));

                // if diff is zero, then the bottom has been reached
                if (diff == 0) {
                    detailKeluhanViewModel.initAspirasiTerkait();
                }
            }
        });
        binding.videoViewContent.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer arg0) {
                // TODO Auto-generated method stub
            }

        });
        binding.videoViewContent.setPlayPauseListener(new CustomVideoView.PlayPauseListener() {

            @Override
            public void onPlay() {
                binding.btnPlayVideo.setVisibility(View.GONE);

            }

            @Override
            public void onPause() {
                binding.btnPlayVideo.setVisibility(View.VISIBLE);
            }
        });
        binding.videoViewContent.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                binding.btnPlayVideo.setVisibility(View.VISIBLE);
            }
        });

        binding.btnPlayVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.btnPlayVideo.setVisibility(View.GONE);
                binding.videoViewContent.start();

            }
        });

        binding.setViewModel(detailKeluhanViewModel);
        return view;
    }

    @Override
    public void onBtnCommentClicked() {
        System.out.println("KEKLIK SIH");
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

//    @Override
//    public void onBackPressed() {
//        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
//            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//        } else {
//            super.onBackPressed();
//        }
//    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        KomentarListAdapter adapter = new KomentarListAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void setupRecyclerViewTerkait(RecyclerView recyclerView) {
        KeluhanListAdapter adapter = new KeluhanListAdapter(this,"terkait");
        recyclerView.setAdapter(adapter);
        final StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        recyclerView.setItemViewCacheSize(10000);
        recyclerView.setHasFixedSize(true);
    }


    @Override
    public void onLongClickKeluhan(int position) {

    }
}
