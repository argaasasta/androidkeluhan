package com.ebdesk.mobile.modulekeluhan.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.ebdesk.mobile.modulekeluhan.KeluhanApplication;
import com.ebdesk.mobile.modulekeluhan.model.ServiceApi;
import com.ebdesk.mobile.modulekeluhan.model.model_aspirasi.AspirasiData;
import com.ebdesk.mobile.modulekeluhan.model.model_category.Category;
import com.ebdesk.mobile.modulekeluhan.model.model_category.CategoryData;
import com.ebdesk.mobile.modulekeluhan.service.UploadService;
import com.ebdesk.mobile.modulekeluhan.util.UtilMethod;
import com.ebdesk.mobile.modulekeluhan.view.dialog.CategoryDialog;
import com.isas.module.getmedia.helpers.Constants;
import com.isas.module.getmedia.helpers.SingleShotLocationProvider;
import com.isas.module.getmedia.model.Image;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

import static android.R.attr.path;
import static android.app.Activity.RESULT_OK;

/**
 * Created by EB-NB21 on 3/13/2017.
 */

public class AddKeluhanViewModel extends BaseObservable implements ViewModel, CategoryDialog.OnPickCategoryListener, SingleShotLocationProvider.LocationCallback {

    private static final String TAG = AddKeluhanViewModel.class.getSimpleName();
    private AspirasiData aspirasiData;
    private Activity activity;
    private Context context;

    private AspirasiData newAspirasiData;
    private Subscription subscription;
    public ObservableField<String> resultPicturePath;
    public ObservableField<String> locationDesc;
    public ObservableInt typeVideoVisibility;
    public ObservableInt typePictureVisibility;
    public ObservableInt sendProgressVisibility;
    public String mediaPath = "";
    public String typeMediaSend = "";
    //ATRIBUT ASPIRASI
    public ObservableArrayList<CategoryData> listCategoryData;

    public String descString = "";

    public String categoryIdSelected = "";
    public ObservableField<String> categoryString;
    public String latitudeString = "";
    public String longitudeString = "";

    public String latitudeStringDefault;
    public String longitudeStringDefault;

    public String createdBy;
    public File fileImage;

    public AddKeluhanViewModel(Activity activity) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        this.resultPicturePath = new ObservableField<>();
        this.locationDesc = new ObservableField<>();
        this.categoryString = new ObservableField<>();
        categoryString.set("PICK CATEGORY");
        this.typeVideoVisibility = new ObservableInt(View.VISIBLE);
        this.typePictureVisibility = new ObservableInt(View.VISIBLE);
        this.sendProgressVisibility = new ObservableInt(View.GONE);
        this.listCategoryData = new ObservableArrayList<>();
        getCategory();
        SingleShotLocationProvider.requestSingleUpdate(activity, this);
    }

    public void selectCategory(View view) {
        CategoryDialog categoryDialogDialog = new CategoryDialog(activity, listCategoryData, this);
        categoryDialogDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        categoryDialogDialog.show();
//        Toast.makeText(activity, "CATEGORY SELECTED", Toast.LENGTH_SHORT).show();
    }

    public void getCategory() {
        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        KeluhanApplication application = KeluhanApplication.get(context);

        application.setServiceApi(ServiceApi.Factory.createServiceInfoKota());
        ServiceApi serviceApi = application.getServiceApi();

        subscription = serviceApi.publicGetCategory(0, 15)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(new Subscriber<Category>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println("ADA ERROR ASPIRASI");
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Category category) {
                        for (int i = 0; i < category.getData().size(); i++) {
                            System.out.println("ISI API CATEGORY" + category.getData().get(i).getName());
                            listCategoryData.add(category.getData().get(i));
                        }
                    }
                });
    }

    public void onSendKeluhan(View view) {
//        context.startActivity(DetailKeluhanActivity.newIntent(context, aspirasiData));
//        Toast.makeText(context, "KELUHAN ANDA DIKIRIM HAHAHAHA", Toast.LENGTH_SHORT).show();
        try {
            if (latitudeString.equals("") || latitudeString.equals("0.0")) {
                latitudeString = latitudeStringDefault;
                longitudeString = longitudeStringDefault;
            }
        } catch (Exception e) {
            Log.e(TAG, "onSendKeluhan: ", e);
            latitudeString = "0.0";
            longitudeString="0.0";
        }

        if (categoryIdSelected.equals("")) {
            Toast.makeText(activity, "Pick Category", Toast.LENGTH_SHORT).show();
        } else if (descString.equals("")) {
            Toast.makeText(activity, "Fill description", Toast.LENGTH_SHORT).show();
        } else {
            if (typeMediaSend.equals("video")) {
                postAspirasiVideo(mediaPath, typeMediaSend);
            } else {
                sendProgressVisibility.set(View.VISIBLE);
                postAspirasiImage(fileImage, typeMediaSend);
            }
        }
    }

    public void postAspirasiImage(File image, String type) {
        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        KeluhanApplication application = KeluhanApplication.get(context);

        application.setServiceApi(ServiceApi.Factory.createServiceAspirasi(ServiceApi.TYPE_STRING));
        ServiceApi serviceApi = application.getServiceApi();

        RequestBody categoryId = RequestBody.create(MediaType.parse("text/plain"), categoryIdSelected);
        RequestBody categoryName = RequestBody.create(MediaType.parse("text/plain"), categoryString.get());
        RequestBody desc = RequestBody.create(MediaType.parse("text/plain"), descString);
        RequestBody latitude = RequestBody.create(MediaType.parse("text/plain"), latitudeString);
        RequestBody longitude = RequestBody.create(MediaType.parse("text/plain"), longitudeString);
        RequestBody createdBy = RequestBody.create(MediaType.parse("text/plain"), "saya");
        RequestBody mediaType = RequestBody.create(MediaType.parse("text/plain"), type);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("photo", image.getName(), RequestBody.create(MediaType.parse("image/*"), image));
        subscription = serviceApi.postUpdateActivity(categoryId, categoryName, desc, latitude, longitude, createdBy, mediaType, filePart)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(new Subscriber<String>() {

                    @Override
                    public void onCompleted() {
                        sendProgressVisibility.set(View.GONE);
                        activity.finish();
                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println("ERROR SEND KELUHAN");
                        e.printStackTrace();
                        activity.finish();
                    }

                    @Override
                    public void onNext(String response) {
                        System.out.println("RESPONSE " + response);
                    }

                });
    }

    public void postAspirasiVideo(String mediaPath, String type) {
        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        Intent mServiceIntent = new Intent(activity.getApplicationContext(), UploadService.class);
        mServiceIntent.putExtra("CATEGORY_ID", categoryIdSelected);
        mServiceIntent.putExtra("CATEGORY_NAME", categoryString.get());
        mServiceIntent.putExtra("DESCRIPTION", descString);
        mServiceIntent.putExtra("LATITUDE", latitudeString);
        mServiceIntent.putExtra("LONGITUDE", longitudeString);
        mServiceIntent.putExtra("CREATED_BY", "saya");
        mServiceIntent.putExtra("MEDIA_TYPE", type);
        mServiceIntent.putExtra("VIDEO_PATH", mediaPath);
        activity.startService(mServiceIntent);
        Toast.makeText(activity, "Video sedang dikirim", Toast.LENGTH_SHORT).show();
        activity.finish();
    }

    public TextWatcher getDescEditTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                descString = charSequence.toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
    }

    @Override
    public void destroy() {
        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        subscription = null;
        context = null;
    }

    public void activityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CODE && resultCode == RESULT_OK && data != null) {//ambil gambar dari galery sebanyak 5
            ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);

            for (int i = 0; i < images.size(); i++) {
                mediaPath = images.get(i).path;
                if (UtilMethod.getMimeType(images.get(i).path).equals("video/mp4")) {
                    System.out.println("PATH VIDEO " + images.get(i).path);
                    resultPicturePath.set(images.get(i).path);
                    typeMediaSend = "video";
                    typeVideoVisibility.set(View.VISIBLE);
                    typePictureVisibility.set(View.GONE);
                } else {
                    typeMediaSend = "image";
                    typeVideoVisibility.set(View.GONE);
                    typePictureVisibility.set(View.VISIBLE);

                    resultPicturePath.set(images.get(i).path);
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    Bitmap bitmap = BitmapFactory.decodeFile(images.get(i).path, bmOptions);
                    System.out.println("mau d kompress jadi berapa ukurannya " + bmOptions.outWidth + " dan " + bmOptions.outHeight);
                    Bitmap bitmapResize = Bitmap.createScaledBitmap(bitmap, bmOptions.outWidth / 8, bmOptions.outHeight / 8, false);
                    File destination = new File(context.getCacheDir(), String.valueOf(System.currentTimeMillis()) + ".jpg");
                    try {
                        destination.createNewFile();
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        bitmapResize.compress(Bitmap.CompressFormat.JPEG, 90/*ignored for PNG*/, bos);
                        byte[] bitmapdata = bos.toByteArray();

                        //write the bytes in file
                        FileOutputStream fos = new FileOutputStream(destination);
                        fos.write(bitmapdata);
                        bos.flush();
                        bos.close();
                        fos.flush();
                        fos.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    fileImage = destination;

                    ExifInterface exif = null;
                    try {
                        exif = new ExifInterface(images.get(i).path);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String LATITUDE = exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE);
                    String LATITUDE_REF = exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF);
                    String LONGITUDE = exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE);
                    String LONGITUDE_REF = exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF);
                    System.out.println("ADA IMAGE YG ADA EXIFNYA " + path + " : " + LATITUDE);
                    Double Latitude, Longitude;
                    if ((LATITUDE != null) && (LATITUDE_REF != null) && (LONGITUDE != null) && (LONGITUDE_REF != null)) {

                        if (LATITUDE_REF.equals("N")) {
                            Latitude = UtilMethod.convertToDegree(LATITUDE);
                        } else {
                            Latitude = 0 - UtilMethod.convertToDegree(LATITUDE);
                        }

                        if (LONGITUDE_REF.equals("E")) {
                            Longitude = UtilMethod.convertToDegree(LONGITUDE);
                        } else {
                            Longitude = 0 - UtilMethod.convertToDegree(LONGITUDE);
                        }
                        System.out.println("BISA DAPET EXIF DI ADD" + images.get(i).path + " LATITUDE " + Latitude + " LONGITUDE " + Longitude);
                        loadStreetName(Latitude + "", Longitude + "");
                        latitudeString = Latitude + "";
                        longitudeString = Longitude + "";

                    }
                }
            }
        }
    }


    private void loadStreetName(String latitude, String longitude) {
        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        KeluhanApplication application = KeluhanApplication.get(context);
        application.setServiceApi(ServiceApi.Factory.createServiceGeocode());
        ServiceApi serviceApi = application.getServiceApi();
        System.out.println("BISA DAPET LOKASI GA DARI GAMBAR " + latitude + "," + longitude);
        subscription = serviceApi.publicGetStreetName(latitude + "," + longitude)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println("ada ERROR dapet lokasi");
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(String s) {
                        System.out.println("BISA DAPET LOKASI DARI GOOGLE" + s);
                        try {
                            JSONObject response = new JSONObject(s);
                            JSONArray json = null;
                            json = response.getJSONArray("results");
                            // Separated values
                            for (int i = 0; i < json.length(); i++) {
                                JSONObject eachObject = json.getJSONObject(i);
                                JSONArray arrayType = eachObject.getJSONArray("types");
                                // formatted address
                                if (arrayType.toString().contains("\"route\"")
                                        || arrayType.toString().contains(
                                        "\"street_address\"")) {
                                    // Toast.makeText(getApplicationContext(),
                                    // eachObject.getString("formatted_address"),
                                    // Toast.LENGTH_SHORT).show();
                                    String textAddress = eachObject
                                            .getString("formatted_address");
                                    locationDesc.set(textAddress);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
    }

    @BindingAdapter({"loadImageBitmap"})
    public static void loadImage(ImageView view, String pathImage) {
        try {
            System.out.println("ADA GA SIH ISINYA " + pathImage);
//            Uri uriFromFile = Uri.fromFile(new File((pathImage)));
//            String decode = uriFromFile.getEncodedPath();
//            Bitmap resultPicture = BitmapFactory.decodeFile(pathImage);
//            view.setImageBitmap(resultPicture);
            Glide.with(view.getContext())
                    .load(pathImage)
                    .placeholder(android.R.drawable.alert_dark_frame).into(view);
        } catch (Exception e) {

        }
    }

    @BindingAdapter({"loadPathVideo"})
    public static void loadPathVideo(VideoView view, String pathVideo) {
        try {
            MediaController mediaController = new MediaController(view.getContext());
            view.setMediaController(mediaController);
            view.setVideoPath(pathVideo);
            view.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setLooping(true);
                }
            });
            view.start();
        } catch (Exception e) {

        }
    }


    @Override
    public void onCategorySelected(CategoryData categoryData) {
        System.out.println("CATEGORY SELECTED " + categoryData.getName());
        categoryIdSelected = categoryData.getId();
        categoryString.set(categoryData.getName());
    }

    @Override
    public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
        latitudeStringDefault = location.latitude + "";
        longitudeStringDefault = location.longitude + "";
        System.out.println("DAPET GA SIH SINGLE SHOOT " + latitudeStringDefault);
    }
}
