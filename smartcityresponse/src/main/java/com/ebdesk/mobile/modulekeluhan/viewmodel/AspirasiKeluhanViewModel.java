package com.ebdesk.mobile.modulekeluhan.viewmodel;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.widget.RecyclerView;

import android.view.View;
import android.widget.Toast;

import com.ebdesk.mobile.modulekeluhan.KeluhanApplication;
import com.ebdesk.mobile.modulekeluhan.R;
import com.ebdesk.mobile.modulekeluhan.adapter.KeluhanListAdapter;
import com.ebdesk.mobile.modulekeluhan.model.ServiceApi;
import com.ebdesk.mobile.modulekeluhan.model.model_aspirasi.Aspirasi;
import com.ebdesk.mobile.modulekeluhan.model.model_aspirasi.AspirasiData;
import com.ebdesk.mobile.modulekeluhan.view.AddKeluhanActivity;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by EB-NB21 on 3/1/2017.
 */

public class AspirasiKeluhanViewModel extends BaseObservable implements ViewModel {

    private Context context;
    private Subscription subscription;
    private String typeList;
    public ObservableArrayList<AspirasiData> listAspirasiData;
    public ObservableInt positionSelected;
    public ObservableInt fabViewVisibility;
    public ObservableField<String> pathShare;

    private OnRefreshKeluhan onRefreshKeluhan;

    public AspirasiKeluhanViewModel(Context context,OnRefreshKeluhan onRefreshKeluhan,String typeList) {
        this.context = context;
        this.typeList = typeList;
        this.onRefreshKeluhan = onRefreshKeluhan;
        listAspirasiData = new ObservableArrayList<>();
        positionSelected = new ObservableInt();
        fabViewVisibility = new ObservableInt(View.INVISIBLE);

        pathShare = new ObservableField<>();
        if (typeList.equals("TERKAIT")) {
            fabViewVisibility.set(View.INVISIBLE);
        } else {
//            initAspirasiTerkait();
            getAspirasi(0);
            fabViewVisibility.set(View.VISIBLE);
        }

    }

    @Override
    public void destroy() {
        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        subscription = null;
        context = null;
    }
    public void initKeluhanFirst() {
        listAspirasiData.clear();
        getAspirasi(0);
    }

    public void getAspirasi(int start){
        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        KeluhanApplication application = KeluhanApplication.get(context);

        application.setServiceApi(ServiceApi.Factory.createServiceInfoKota());
        ServiceApi serviceApi = application.getServiceApi();

        subscription = serviceApi.publicGetAspirasi(start,10)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(new Subscriber<Aspirasi>() {
                    @Override
                    public void onCompleted() {
                        onRefreshKeluhan.onItemsLoadComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println("ADA ERROR ASPIRASI");
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Aspirasi aspirasi) {
                        for (int i = 0; i < aspirasi.getData().size(); i++) {
                            System.out.println("ISI API ASPIRASI" + aspirasi.getData().get(i).getDesc());
                            listAspirasiData.add(aspirasi.getData().get(i));
                        }
                        notifyChange();
                    }
                });
    }

    @BindingAdapter({"loadList"})
    public static void loadListRecycle(RecyclerView view, ObservableArrayList<AspirasiData> listAspirasiData) {
        KeluhanListAdapter adapter =
                (KeluhanListAdapter) view.getAdapter();
        adapter.setListAspirasiData(listAspirasiData);
        adapter.notifyDataSetChanged();
    }

    public void onItemShareClick(View view) {
        Toast.makeText(context, "POSITION " + positionSelected.get(), Toast.LENGTH_SHORT).show();
    }

    public void imagePathSelected(int position){
        pathShare.set(listAspirasiData.get(position).getFilePath());
    }

    public void onAddKeluhan(View view) {
        LocationManager lm = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setMessage("GPS not enable");
            dialog.setPositiveButton("Set GPS", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        }else{
            context.startActivity(AddKeluhanActivity.newIntent(context));
        }

    }

    public interface OnRefreshKeluhan{
        public void onItemsLoadComplete();
    }

}
