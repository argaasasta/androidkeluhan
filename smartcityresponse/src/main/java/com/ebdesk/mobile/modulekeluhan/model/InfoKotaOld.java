package com.ebdesk.mobile.modulekeluhan.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by EB-NB21 on 3/10/2017.
 */

public class InfoKotaOld implements Parcelable {

    public String id;
    public String creator;
    public String creatorPicture;
    public String desc;
    public String descPic;
    public String date;

    public InfoKotaOld(String id, String creator, String creatorPicture, String desc, String descPic, String date) {
        this.id = id;
        this.creator = creator;
        this.creatorPicture = creatorPicture;
        this.desc = desc;
        this.descPic = descPic;
        this.date = date;
    }


    protected InfoKotaOld(Parcel in) {
        this.id = in.readString();
        this.creator = in.readString();
        this.creatorPicture = in.readString();
        this.desc = in.readString();
        this.descPic = in.readString();
        this.date = in.readString();
    }


    public static final Creator<InfoKotaOld> CREATOR = new Creator<InfoKotaOld>() {
        @Override
        public InfoKotaOld createFromParcel(Parcel in) {
            return new InfoKotaOld(in);
        }

        @Override
        public InfoKotaOld[] newArray(int size) {
            return new InfoKotaOld[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.id);
        parcel.writeString(this.creator);
        parcel.writeString(this.creatorPicture);
        parcel.writeString(this.desc);
        parcel.writeString(this.descPic);
        parcel.writeString(this.date);
    }
}
