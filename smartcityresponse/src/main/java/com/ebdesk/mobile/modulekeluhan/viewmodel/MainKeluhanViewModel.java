package com.ebdesk.mobile.modulekeluhan.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.ebdesk.mobile.modulekeluhan.adapter.MyPagerAdapter;
import com.ebdesk.mobile.modulekeluhan.view.AspirasiKeluhanFragment;

/**
 * Created by EB-NB21 on 3/10/2017.
 */

public class MainKeluhanViewModel extends BaseObservable implements ViewModel {

    private Context context;
    private FragmentManager fragmentManager;
    public MainKeluhanViewModel(Context context,FragmentManager fragmentManager) {
        this.context = context;
        this.fragmentManager = fragmentManager;

    }

    @Override
    public void destroy() {

    }

    @BindingAdapter({"loadPagerAdapter"})
    public static void loadPagerAdapter(final ViewPager view, FragmentStatePagerAdapter pagerAdapter) {
        view.setAdapter(pagerAdapter);
    }

    public MyPagerAdapter getPagerAdapter(){
        return new MyPagerAdapter(fragmentManager);
    }

}
