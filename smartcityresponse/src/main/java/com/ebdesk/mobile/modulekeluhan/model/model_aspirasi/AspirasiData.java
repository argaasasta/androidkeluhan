
package com.ebdesk.mobile.modulekeluhan.model.model_aspirasi;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AspirasiData implements Parcelable
{

    @SerializedName("id_aspirasi")
    @Expose
    private String idAspirasi;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_time")
    @Expose
    private String createdTime;
    @SerializedName("file_path")
    @Expose
    private String filePath;
    @SerializedName("thumb_path")
    @Expose
    private String thumbPath;
    @SerializedName("location_desc")
    @Expose
    private String locationDesc;
    @SerializedName("location_adm_area_3")
    @Expose
    private String locationAdmArea3;
    @SerializedName("location_adm_area_2")
    @Expose
    private String locationAdmArea2;
    @SerializedName("location_adm_area_1")
    @Expose
    private String locationAdmArea1;
    @SerializedName("source_type")
    @Expose
    private String sourceType;
    @SerializedName("banned")
    @Expose
    private String banned;
    public final static Parcelable.Creator<AspirasiData> CREATOR = new Creator<AspirasiData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public AspirasiData createFromParcel(Parcel in) {
            AspirasiData instance = new AspirasiData();
            instance.idAspirasi = ((String) in.readValue((String.class.getClassLoader())));
            instance.categoryName = ((String) in.readValue((String.class.getClassLoader())));
            instance.desc = ((String) in.readValue((String.class.getClassLoader())));
            instance.latitude = ((String) in.readValue((String.class.getClassLoader())));
            instance.longitude = ((String) in.readValue((String.class.getClassLoader())));
            instance.createdBy = ((String) in.readValue((String.class.getClassLoader())));
            instance.createdTime = ((String) in.readValue((String.class.getClassLoader())));
            instance.filePath = ((String) in.readValue((String.class.getClassLoader())));
            instance.thumbPath = ((String) in.readValue((String.class.getClassLoader())));
            instance.locationDesc = ((String) in.readValue((String.class.getClassLoader())));
            instance.locationAdmArea3 = ((String) in.readValue((String.class.getClassLoader())));
            instance.locationAdmArea2 = ((String) in.readValue((String.class.getClassLoader())));
            instance.locationAdmArea1 = ((String) in.readValue((String.class.getClassLoader())));
            instance.sourceType = ((String) in.readValue((String.class.getClassLoader())));
            instance.banned = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public AspirasiData[] newArray(int size) {
            return (new AspirasiData[size]);
        }

    }
            ;

    /**
     * No args constructor for use in serialization
     *
     */
    public AspirasiData() {
    }

    /**
     *
     * @param locationAdmArea1
     * @param sourceType
     * @param locationDesc
     * @param desc
     * @param thumbPath
     * @param createdTime
     * @param filePath
     * @param banned
     * @param categoryName
     * @param createdBy
     * @param longitude
     * @param idAspirasi
     * @param latitude
     * @param locationAdmArea2
     * @param locationAdmArea3
     */
    public AspirasiData(String idAspirasi, String categoryName, String desc, String latitude, String longitude, String createdBy, String createdTime, String filePath, String thumbPath, String locationDesc, String locationAdmArea3, String locationAdmArea2, String locationAdmArea1, String sourceType, String banned) {
        super();
        this.idAspirasi = idAspirasi;
        this.categoryName = categoryName;
        this.desc = desc;
        this.latitude = latitude;
        this.longitude = longitude;
        this.createdBy = createdBy;
        this.createdTime = createdTime;
        this.filePath = filePath;
        this.thumbPath = thumbPath;
        this.locationDesc = locationDesc;
        this.locationAdmArea3 = locationAdmArea3;
        this.locationAdmArea2 = locationAdmArea2;
        this.locationAdmArea1 = locationAdmArea1;
        this.sourceType = sourceType;
        this.banned = banned;
    }

    public String getIdAspirasi() {
        return idAspirasi;
    }

    public void setIdAspirasi(String idAspirasi) {
        this.idAspirasi = idAspirasi;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getFilePath() {
        if(filePath.contains("http")){
            return filePath;
        }else {
            return "http://103.43.128.142:8880/" + filePath;
        }
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getThumbPath() {
        return thumbPath;
    }

    public void setThumbPath(String thumbPath) {
        this.thumbPath = thumbPath;
    }

    public String getLocationDesc() {
        return locationDesc;
    }

    public void setLocationDesc(String locationDesc) {
        this.locationDesc = locationDesc;
    }

    public String getLocationAdmArea3() {
        return locationAdmArea3;
    }

    public void setLocationAdmArea3(String locationAdmArea3) {
        this.locationAdmArea3 = locationAdmArea3;
    }

    public String getLocationAdmArea2() {
        return locationAdmArea2;
    }

    public void setLocationAdmArea2(String locationAdmArea2) {
        this.locationAdmArea2 = locationAdmArea2;
    }

    public String getLocationAdmArea1() {
        return locationAdmArea1;
    }

    public void setLocationAdmArea1(String locationAdmArea1) {
        this.locationAdmArea1 = locationAdmArea1;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getBanned() {
        return banned;
    }

    public void setBanned(String banned) {
        this.banned = banned;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(idAspirasi);
        dest.writeValue(categoryName);
        dest.writeValue(desc);
        dest.writeValue(latitude);
        dest.writeValue(longitude);
        dest.writeValue(createdBy);
        dest.writeValue(createdTime);
        dest.writeValue(filePath);
        dest.writeValue(thumbPath);
        dest.writeValue(locationDesc);
        dest.writeValue(locationAdmArea3);
        dest.writeValue(locationAdmArea2);
        dest.writeValue(locationAdmArea1);
        dest.writeValue(sourceType);
        dest.writeValue(banned);
    }

    public int describeContents() {
        return  0;
    }
}
