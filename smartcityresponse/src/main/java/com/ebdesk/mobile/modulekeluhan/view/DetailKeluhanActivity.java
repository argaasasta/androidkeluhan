package com.ebdesk.mobile.modulekeluhan.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;


import com.ebdesk.mobile.modulekeluhan.R;

import com.ebdesk.mobile.modulekeluhan.model.model_aspirasi.AspirasiData;


public class DetailKeluhanActivity extends AppCompatActivity{
    private static final String EXTRA_KELUHAN = "EXTRA_KELUHAN";

    public static Intent newIntent(Context context, AspirasiData aspirasiData) {
        Intent intent = new Intent(context.getString(R.string.action_info_kota_detail));
        intent.putExtra(context.getString(R.string.key_extra_keluhan), aspirasiData);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_keluhan);
        AspirasiData aspirasiData = getIntent().getParcelableExtra(getString(R.string.key_extra_keluhan));//ga boleh nih

        DetailKeluhanFragment myf = DetailKeluhanFragment.newInstance(aspirasiData);
        getSupportFragmentManager().beginTransaction().replace(R.id.detail_keluhan_content,  myf).commit();
    }

}
