
package com.ebdesk.mobile.modulekeluhan.model.model_info_kota;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemInfoKota implements Parcelable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("info_image")
    @Expose
    private String infoImage;
    @SerializedName("media_image")
    @Expose
    private String mediaImage;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("source_type")
    @Expose
    private String sourceType;
    @SerializedName("pub_date")
    @Expose
    private String pubDate;
    public final static Creator<ItemInfoKota> CREATOR = new Creator<ItemInfoKota>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ItemInfoKota createFromParcel(Parcel in) {
            ItemInfoKota instance = new ItemInfoKota();
            instance.id = ((String) in.readValue((String.class.getClassLoader())));
            instance.name = ((String) in.readValue((String.class.getClassLoader())));
            instance.title = ((String) in.readValue((String.class.getClassLoader())));
            instance.desc = ((String) in.readValue((String.class.getClassLoader())));
            instance.infoImage = ((String) in.readValue((String.class.getClassLoader())));
            instance.mediaImage = ((String) in.readValue((String.class.getClassLoader())));
            instance.link = ((String) in.readValue((String.class.getClassLoader())));
            instance.sourceType = ((String) in.readValue((String.class.getClassLoader())));
            instance.pubDate = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public ItemInfoKota[] newArray(int size) {
            return (new ItemInfoKota[size]);
        }

    }
    ;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ItemInfoKota() {
    }

    /**
     * 
     * @param id
     * @param pubDate
     * @param sourceType
     * @param title
     * @param desc
     * @param link
     * @param name
     * @param mediaImage
     * @param infoImage
     */
    public ItemInfoKota(String id, String name, String title, String desc, String infoImage, String mediaImage, String link, String sourceType, String pubDate) {
        super();
        this.id = id;
        this.name = name;
        this.title = title;
        this.desc = desc;
        this.infoImage = infoImage;
        this.mediaImage = mediaImage;
        this.link = link;
        this.sourceType = sourceType;
        this.pubDate = pubDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getInfoImage() {
        return infoImage;
    }

    public void setInfoImage(String infoImage) {
        this.infoImage = infoImage;
    }

    public String getMediaImage() {
        return mediaImage;
    }

    public void setMediaImage(String mediaImage) {
        this.mediaImage = mediaImage;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(title);
        dest.writeValue(desc);
        dest.writeValue(infoImage);
        dest.writeValue(mediaImage);
        dest.writeValue(link);
        dest.writeValue(sourceType);
        dest.writeValue(pubDate);
    }

    public int describeContents() {
        return  0;
    }

}
