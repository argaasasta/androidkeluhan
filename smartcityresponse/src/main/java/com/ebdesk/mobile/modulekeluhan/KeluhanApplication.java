package com.ebdesk.mobile.modulekeluhan;

import android.app.Application;
import android.content.Context;

import com.ebdesk.mobile.modulekeluhan.model.ServiceApi;

import rx.Scheduler;
import rx.schedulers.Schedulers;

/**
 * Created by EB-NB21 on 3/1/2017.
 */

public class KeluhanApplication {

    private ServiceApi serviceApi;
    private Scheduler defaultSubscribeScheduler;
    private static KeluhanApplication keluhanApplication;

    public synchronized static KeluhanApplication get(Context context) {
        if (keluhanApplication == null) {
            keluhanApplication = new KeluhanApplication();
        }
        return keluhanApplication;
    }

    public ServiceApi getServiceApi() {
        if (serviceApi == null) {
            return null;
        }else{
            return serviceApi;
        }

    }

//    public ServiceApi getServiceApiInfoKota() {
//        if (serviceApi == null) {
//            serviceApi = ServiceApi.Factory.createServiceInfoKota();
//        }
//        return serviceApi;
//    }
//
//    public ServiceApi getServiceAspirasi() {
//        if (serviceApi == null) {
//            serviceApi = ServiceApi.Factory.createServiceAspirasi();
//        }
//        return serviceApi;
//    }

    //For setting mocks during testing
    public void setServiceApi(ServiceApi serviceApi) {
        this.serviceApi = serviceApi;
    }

    public Scheduler defaultSubscribeScheduler() {
        if (defaultSubscribeScheduler == null) {
            defaultSubscribeScheduler = Schedulers.io();
        }
        return defaultSubscribeScheduler;
    }

    //User to change scheduler from tests
    public void setDefaultSubscribeScheduler(Scheduler scheduler) {
        this.defaultSubscribeScheduler = scheduler;
    }
}
