package com.ebdesk.mobile.modulekeluhan.viewmodel.viewmodelitem;

import android.content.Context;
import android.databinding.BaseObservable;

import com.ebdesk.mobile.modulekeluhan.model.Komentar;
import com.ebdesk.mobile.modulekeluhan.viewmodel.ViewModel;

/**
 * Created by EB-NB21 on 3/3/2017.
 */

public class ItemKomentarViewModel extends BaseObservable implements ViewModel {
    private Komentar komentar;
    private Context context;

    public ItemKomentarViewModel(Context context, Komentar komentar) {
        this.context = context;
        this.komentar = komentar;
    }

    public void setKomentar(Komentar komentar) {
        this.komentar = komentar;
        notifyChange();
    }


    public String getId() {
        return komentar.id;
    }

    public String getCreatorName() {
        return komentar.creatorName;
    }

    public String getCreatorPicture() {
        return komentar.creatorPicture;
    }

    public String getKomentarDesc() {
        return komentar.komentarDesc;
    }

    public String getKomentarImage() {
        return komentar.komentarImage;
    }

    public String getKomentarTimeStamp() {
        return komentar.komentarTimeStamp;
    }

    public String getStatus() {
        if(komentar.status == 0){
            return "Sending";
        }else{
            return "Done";
        }
    }

    @Override
    public void destroy() {

    }
}
