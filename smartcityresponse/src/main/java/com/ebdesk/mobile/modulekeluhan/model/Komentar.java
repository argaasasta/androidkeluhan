package com.ebdesk.mobile.modulekeluhan.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by EB-NB21 on 3/3/2017.
 */

public class Komentar implements Parcelable {

    public String id;
    public String creatorName;
    public String creatorPicture;
    public String komentarImage;
    public String komentarDesc;
    public String komentarTimeStamp;
    public int status;


    public Komentar() {
    }

    public Komentar(String id, String creatorName, String creatorPicture, String komentarDesc, String komentarImage, String komentarTimeStamp, int status) {
        this.id = id;
        this.creatorName = creatorName;
        this.creatorPicture = creatorPicture;
        this.komentarDesc = komentarDesc;
        this.komentarImage = komentarImage;
        this.komentarTimeStamp = komentarTimeStamp;
        this.status = status;
    }


    protected Komentar(Parcel in) {
        this.id = in.readString();
        this.creatorName = in.readString();
        this.creatorPicture = in.readString();
        this.komentarDesc = in.readString();
        this.komentarImage = in.readString();
        this.komentarTimeStamp = in.readString();
        this.status = in.readInt();

    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(this.id);
        parcel.writeString(this.creatorName);
        parcel.writeString(this.creatorPicture);
        parcel.writeString(this.komentarDesc);
        parcel.writeString(this.komentarImage);
        parcel.writeString(this.komentarTimeStamp);
        parcel.writeInt(this.status);

    }

    public static final Creator<Komentar> CREATOR = new Creator<Komentar>() {
        @Override
        public Komentar createFromParcel(Parcel in) {
            return new Komentar(in);
        }

        @Override
        public Komentar[] newArray(int size) {
            return new Komentar[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }
}