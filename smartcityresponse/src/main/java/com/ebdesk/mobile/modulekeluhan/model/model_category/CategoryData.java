
package com.ebdesk.mobile.modulekeluhan.model.model_category;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryData implements Parcelable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("active")
    @Expose
    private Integer active;
    @SerializedName("is_tps")
    @Expose
    private Integer isTps;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("update_by")
    @Expose
    private String updateBy;
    @SerializedName("update_time")
    @Expose
    private String updateTime;
    @SerializedName("path_photo")
    @Expose
    private String pathPhoto;
    @SerializedName("marker_status_img")
    @Expose
    private String markerStatusImg;
    public final static Creator<CategoryData> CREATOR = new Creator<CategoryData>() {


        @SuppressWarnings({
            "unchecked"
        })
        public CategoryData createFromParcel(Parcel in) {
            CategoryData instance = new CategoryData();
            instance.id = ((String) in.readValue((String.class.getClassLoader())));
            instance.name = ((String) in.readValue((String.class.getClassLoader())));
            instance.desc = ((String) in.readValue((String.class.getClassLoader())));
            instance.active = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.isTps = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.color = ((String) in.readValue((String.class.getClassLoader())));
            instance.updateBy = ((String) in.readValue((String.class.getClassLoader())));
            instance.updateTime = ((String) in.readValue((String.class.getClassLoader())));
            instance.pathPhoto = ((String) in.readValue((String.class.getClassLoader())));
            instance.markerStatusImg = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public CategoryData[] newArray(int size) {
            return (new CategoryData[size]);
        }

    }
    ;

    /**
     * No args constructor for use in serialization
     * 
     */
    public CategoryData() {
    }

    /**
     * 
     * @param updateBy
     * @param id
     * @param desc
     * @param updateTime
     * @param color
     * @param name
     * @param active
     * @param pathPhoto
     * @param markerStatusImg
     * @param isTps
     */
    public CategoryData(String id, String name, String desc, Integer active, Integer isTps, String color, String updateBy, String updateTime, String pathPhoto, String markerStatusImg) {
        super();
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.active = active;
        this.isTps = isTps;
        this.color = color;
        this.updateBy = updateBy;
        this.updateTime = updateTime;
        this.pathPhoto = pathPhoto;
        this.markerStatusImg = markerStatusImg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getIsTps() {
        return isTps;
    }

    public void setIsTps(Integer isTps) {
        this.isTps = isTps;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getPathPhoto() {
        return pathPhoto;
    }

    public void setPathPhoto(String pathPhoto) {
        this.pathPhoto = pathPhoto;
    }

    public String getMarkerStatusImg() {
        return markerStatusImg;
    }

    public void setMarkerStatusImg(String markerStatusImg) {
        this.markerStatusImg = markerStatusImg;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(desc);
        dest.writeValue(active);
        dest.writeValue(isTps);
        dest.writeValue(color);
        dest.writeValue(updateBy);
        dest.writeValue(updateTime);
        dest.writeValue(pathPhoto);
        dest.writeValue(markerStatusImg);
    }

    public int describeContents() {
        return  0;
    }

}
