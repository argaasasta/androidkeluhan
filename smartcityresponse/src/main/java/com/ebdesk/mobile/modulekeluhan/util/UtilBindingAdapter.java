package com.ebdesk.mobile.modulekeluhan.util;

import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.BitmapEncoder;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.ebdesk.mobile.modulekeluhan.adapter.KomentarListAdapter;
import com.ebdesk.mobile.modulekeluhan.R;
import com.ebdesk.mobile.modulekeluhan.model.Komentar;

/**
 * Created by EB-NB21 on 3/2/2017.
 */

public class UtilBindingAdapter {

    @BindingAdapter({"loadImage"})
    public static void loadImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext())
                .load(imageUrl).asBitmap().centerCrop()
                .placeholder(R.drawable.placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new jp.wasabeef.glide.transformations.RoundedCornersTransformation(view.getContext(),10,0))
                .error(R.drawable.placeholder)
                .encoder(new BitmapEncoder(Bitmap.CompressFormat.PNG, 100))
                .into(view);
    }

    @BindingAdapter({"loadImageKeluhanList"})
    public static void loadImageKeluhanList(ImageView view, String imageUrl) {
        Glide.with(view.getContext())
                .load(imageUrl).asBitmap().centerCrop()
                .placeholder(R.drawable.placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .override(400, 300)
                .fitCenter()
                .transform(new jp.wasabeef.glide.transformations.RoundedCornersTransformation(view.getContext(),10,0))
                .error(R.drawable.placeholder)
                .encoder(new BitmapEncoder(Bitmap.CompressFormat.JPEG, 100))
                .into(view);
    }

    @BindingAdapter({"loadVideo"})
    public static void loadVideo(VideoView view, String videoUrl) {
        MediaController videoMediaController = new MediaController(view.getContext());
        view.setVideoPath(videoUrl);
        videoMediaController.setMediaPlayer(view);
        view.setMediaController(videoMediaController);
        view.seekTo(100);
    }

    @BindingAdapter({"loadImageProfile"})
    public static void loadImageProfile(ImageView view, String imageUrl) {
        Glide.with(view.getContext())
                .load(imageUrl).asBitmap().centerCrop()
                .placeholder(R.drawable.placeholder_avatar)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new jp.wasabeef.glide.transformations.RoundedCornersTransformation(view.getContext(),10,0))
                .error(R.drawable.placeholder_avatar)
                .encoder(new BitmapEncoder(Bitmap.CompressFormat.PNG, 100))
                .into(view);
    }

    @BindingAdapter({"loadCircleImage"})
    public static void loadCircleImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext())
                .load(imageUrl).asBitmap().centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .into(new BitmapImageViewTarget(view) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(view.getContext().getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                view.setImageDrawable(circularBitmapDrawable);
            }
        });
    }

    @BindingAdapter({"loadBackgroundImage"})
    public static void loadBackImage(final CardView view, String imageUrl) {
        Glide.with(view.getContext()).load(imageUrl).asBitmap().into(new SimpleTarget<Bitmap>(view.getWidth(), view.getHeight()) {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                Drawable drawable = new BitmapDrawable(resource);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    view.setBackground(drawable);
                }
            }
        });
    }

    @BindingAdapter({"keluhanStatus"})
    public static void loadStatus(ImageView view, int status) {
        if (status == 1) {
            view.setImageResource(R.drawable.btn_red_waiting);
        } else if (status == 2) {
            view.setImageResource(R.drawable.btn_yellow_progress);
        } else if (status == 3) {
            view.setImageResource(R.drawable.btn_green_done);
        }
    }

    @BindingAdapter({"keluhanStatusBg"})
    public static void loadStatus(LinearLayout view, int status) {
        float scale = view.getContext().getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (10*scale + 0.5f);
        view.setPadding(15,dpAsPixels,15,dpAsPixels);
        if (status == 1) {
            view.setBackgroundResource(R.drawable.btn_red_waiting);
        } else if (status == 2) {
            view.setBackgroundResource(R.drawable.btn_yellow_progress);
        } else if (status == 3) {
            view.setBackgroundResource(R.drawable.btn_green_done);
        }
    }

    @BindingAdapter({"streetViewLatitude","streetViewLongitude"})
    public static void loadStreetPreview(ImageView view,String latitude,String longitude) {
//        String latitude = "46.414382";
//        String longitude = "10.013988";
        String imageUrl ="https://maps.googleapis.com/maps/api/streetview?size=600x300&location="+latitude+","+longitude+"&heading=151.78&pitch=-0.76&key=AIzaSyAITf79BW_eHX8_61B7XStwvEC2sFty9c8";
        Glide.with(view.getContext())
                .load(imageUrl).asBitmap().centerCrop()
                .placeholder(R.drawable.placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new RoundedCornersTransformation(view.getContext(),10, 0))
                .error(R.drawable.placeholder)
                .into(view);
    }

    @BindingAdapter({"loadListKomentar"})
    public static void loadListKomentarRecycle(RecyclerView view, ObservableArrayList<Komentar> listKomentar) {
        KomentarListAdapter adapter =
                (KomentarListAdapter) view.getAdapter();
        System.out.println("DILAKUIN GA "+listKomentar.size());
        adapter.setListKomentar(listKomentar);
        adapter.notifyDataSetChanged();
    }


}
