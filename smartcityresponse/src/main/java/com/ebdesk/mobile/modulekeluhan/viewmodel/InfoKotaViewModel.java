package com.ebdesk.mobile.modulekeluhan.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.support.v7.widget.RecyclerView;

import com.ebdesk.mobile.modulekeluhan.KeluhanApplication;
import com.ebdesk.mobile.modulekeluhan.adapter.InfoKotaListAdapter;
import com.ebdesk.mobile.modulekeluhan.model.ServiceApi;
import com.ebdesk.mobile.modulekeluhan.model.model_info_kota.InfoKota;
import com.ebdesk.mobile.modulekeluhan.model.model_info_kota.ItemInfoKota;

import java.util.ArrayList;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by EB-NB21 on 3/10/2017.
 */

public class InfoKotaViewModel extends BaseObservable implements ViewModel {
    private Context context;
    private Subscription subscription;

    public ObservableArrayList<ItemInfoKota> listItemInfoKota;
    private OnRefreshInfoKota onRefreshInfoKota;

    public InfoKotaViewModel(Context context,OnRefreshInfoKota onRefreshInfoKota) {
        this.context = context;
        this.onRefreshInfoKota = onRefreshInfoKota;
        listItemInfoKota = new ObservableArrayList<>();
//        initDummyListItemKota();
        loadInfoKota(0, 10);
    }

    @Override
    public void destroy() {

    }
    public void initInfoKotaFirst() {
        listItemInfoKota.clear();
        loadInfoKota(0,10);
    }

    private void initDummyListItemKota() {
        ArrayList<String> imgs = new ArrayList<>();
        imgs.add("https://images-na.ssl-images-amazon.com/images/G/01/aplusautomation/vendorimages/65fa961e-8f22-4fe6-a420-3c3c26dd2953.jpg._CB289161999__SL300__.jpg");
        imgs.add("http://www.bmstores.co.uk/images/dmBlog/imgMain/Minions-THUMBNAIL1.png");
        imgs.add("http://corporate.comcast.com/images/minions-thumbnail.jpg");
        imgs.add("https://d6u22qyv3ngwz.cloudfront.net/ad/7QsO/fruitsnackia-minions-small-1.jpg");
        imgs.add("https://upload.wikimedia.org/wikipedia/en/4/4d/Minions.png");
        imgs.add("http://www.mytinyphone.com/uploads/users/twifranny/584272.jpg");
        imgs.add("https://d1nqx6es26drid.cloudfront.net/app/uploads/2015/07/03111621/Banana-Pie-Minions-Trailer.jpg");
        imgs.add("http://www.drodd.com/images14/Minions26.jpg");
        imgs.add("https://s-media-cache-ak0.pinimg.com/originals/b7/9e/be/b79ebe4db5dcef9163603ca8068894ae.png");
        imgs.add("https://s-media-cache-ak0.pinimg.com/736x/05/fd/43/05fd43540d22229c114755d878616a15.jpg");
        imgs.add("https://upload.wikimedia.org/wikipedia/en/4/4d/Minions.png");
        imgs.add("http://www.mytinyphone.com/uploads/users/twifranny/584272.jpg");
        imgs.add("https://d1nqx6es26drid.cloudfront.net/app/uploads/2015/07/03111621/Banana-Pie-Minions-Trailer.jpg");
    }

    public void loadInfoKota(int start, int row) {
        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        KeluhanApplication application = KeluhanApplication.get(context);
        application.setServiceApi(ServiceApi.Factory.createServiceInfoKota());
        ServiceApi serviceApi = application.getServiceApi();
        subscription = serviceApi.publicGetInfoKota(start, row)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(new Subscriber<InfoKota>() {
                    @Override
                    public void onCompleted() {
                        onRefreshInfoKota.onItemsLoadComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println("ADA ERROR ");
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(InfoKota infoKota) {
                        for (int i = 0; i < infoKota.getData().size(); i++) {
                            System.out.println("ISI API INFO KOTA " + infoKota.getData().get(i).getName());
                            listItemInfoKota.add(infoKota.getData().get(i));
                        }
                    }
                });
    }

    @BindingAdapter({"loadListInfoKota"})
    public static void loadListRecycle(RecyclerView view, ObservableArrayList<ItemInfoKota> listItemInfoKota) {
        InfoKotaListAdapter adapter =
                (InfoKotaListAdapter) view.getAdapter();
        adapter.setListInfoKota(listItemInfoKota);
        adapter.notifyDataSetChanged();
    }

    public interface OnRefreshInfoKota{
        public void onItemsLoadComplete();
    }
}
