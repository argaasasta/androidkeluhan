
package com.ebdesk.mobile.modulekeluhan.model.model_info_kota;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InfoKota implements Parcelable
{

    @SerializedName("data")
    @Expose
    private List<ItemInfoKota> data = null;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    public final static Creator<InfoKota> CREATOR = new Creator<InfoKota>() {


        @SuppressWarnings({
            "unchecked"
        })
        public InfoKota createFromParcel(Parcel in) {
            InfoKota instance = new InfoKota();
            in.readList(instance.data, (com.ebdesk.mobile.modulekeluhan.model.model_info_kota.ItemInfoKota.class.getClassLoader()));
            instance.status = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public InfoKota[] newArray(int size) {
            return (new InfoKota[size]);
        }

    }
    ;

    /**
     * No args constructor for use in serialization
     * 
     */
    public InfoKota() {
    }

    /**
     * 
     * @param message
     * @param status
     * @param data
     */
    public InfoKota(List<ItemInfoKota> data, Boolean status, String message) {
        super();
        this.data = data;
        this.status = status;
        this.message = message;
    }

    public List<ItemInfoKota> getData() {
        return data;
    }

    public void setData(List<ItemInfoKota> data) {
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(data);
        dest.writeValue(status);
        dest.writeValue(message);
    }

    public int describeContents() {
        return  0;
    }

}
