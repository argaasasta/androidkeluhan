package com.ebdesk.mobile.modulekeluhan.view;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebdesk.mobile.modulekeluhan.R;
import com.ebdesk.mobile.modulekeluhan.adapter.KeluhanListAdapter;
import com.ebdesk.mobile.modulekeluhan.databinding.FragmentAspirasiKeluhanBinding;
import com.ebdesk.mobile.modulekeluhan.util.EndlessRecyclerViewScrollListener;
import com.ebdesk.mobile.modulekeluhan.viewmodel.AspirasiKeluhanViewModel;

public class AspirasiKeluhanFragment extends Fragment implements KeluhanListAdapter.AdapterKeluhanListener,AspirasiKeluhanViewModel.OnRefreshKeluhan {

    private FragmentAspirasiKeluhanBinding binding;
    private AspirasiKeluhanViewModel aspirasiKeluhanViewModel;

    public AspirasiKeluhanFragment() {
    }

    public static AspirasiKeluhanFragment newInstance(String type) {
        AspirasiKeluhanFragment fragment = new AspirasiKeluhanFragment();
        Bundle args = new Bundle();
        args.putString("TYPE_LIST", type);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_aspirasi_keluhan, container, false);
        View view = binding.getRoot();
        String typeList = getArguments().getString("TYPE_LIST");
        aspirasiKeluhanViewModel = new AspirasiKeluhanViewModel(getActivity(),this,typeList);
        setupRecyclerView(binding.rvKeluhanList);

        binding.setViewModel(aspirasiKeluhanViewModel);

//        SnapHelper snapHelper = new LinearSnapHelper();
//        snapHelper.attachToRecyclerView(binding.rvKeluhanList);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        aspirasiKeluhanViewModel.destroy();
    }


    private void setupRecyclerView(RecyclerView recyclerView) {
        KeluhanListAdapter adapter = new KeluhanListAdapter(this,"aspirasi");
        adapter.setAspirasiViewModel(aspirasiKeluhanViewModel);
        recyclerView.setAdapter(adapter);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        EndlessRecyclerViewScrollListener scrollListener;
        scrollListener = new EndlessRecyclerViewScrollListener(staggeredGridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                System.out.println("HMMMMM"+page);
                aspirasiKeluhanViewModel.getAspirasi(totalItemsCount);
            }
        };
        // Adds the scroll listener to RecyclerView
        recyclerView.addOnScrollListener(scrollListener);
        recyclerView.setItemViewCacheSize(10000);
        recyclerView.setHasFixedSize(true);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
        binding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                aspirasiKeluhanViewModel.initKeluhanFirst();
            }
        });

    }
    @Override
    public void onItemsLoadComplete() {
        binding.swipeRefreshLayout.setRefreshing(false);
    }


    @Override
    public void onLongClickKeluhan(int position) {
        binding.layoutShare.setVisibility(View.VISIBLE);
        binding.layoutShare.setClickable(true);
        aspirasiKeluhanViewModel.imagePathSelected(position);
        binding.layoutShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animeFabFly(1);
                binding.layoutShare.setVisibility(View.GONE);
                binding.layoutShare.setClickable(false);
            }
        });
        animeFabFly(0);
    }

    private void animeFabFly(int type){
        int X0,Y0,X1,Y1,X2,Y2;
        if(type==0){
            X0 = -150;
            Y0 = -70;
            Y1 = -150;
            X2 = 150;
            Y2 = -70;
        }else{
            X0 = 0;
            Y0 = 0;
            Y1 = 0;
            X2 = 0;
            Y2 = 0;

//            ObjectAnimator animAlphaBack = ObjectAnimator.ofFloat(binding.layoutShare, "alpha", 1f, 0f);
//            animatorSet.play(animAlphaBack);
//            animatorSet.start();
        }

        ObjectAnimator animAlpha = ObjectAnimator.ofFloat(binding.fab1, "alpha", 0f, 1f);
        ObjectAnimator animX1 = ObjectAnimator.ofFloat(binding.fab1,"translationX",X0);
        animX1.setDuration(1000);
        ObjectAnimator animY1 = ObjectAnimator.ofFloat(binding.fab1,"translationY",Y0);
        animY1.setDuration(1000);
        AnimatorSet animatorSet1 = new AnimatorSet();
        animatorSet1.setTarget(binding.fab1);
        animatorSet1.play(animX1).with(animY1).with(animAlpha);
        animatorSet1.start();

        ObjectAnimator animAlpha2 = ObjectAnimator.ofFloat(binding.fab2, "alpha", 0f, 1f);
        ObjectAnimator animX2 = ObjectAnimator.ofFloat(binding.fab2,"translationX",0);
        animX2.setDuration(1000);
        ObjectAnimator animY2 = ObjectAnimator.ofFloat(binding.fab2,"translationY",Y1);
        animY2.setDuration(1000);
        AnimatorSet animatorSet2 = new AnimatorSet();
        animatorSet2.setTarget(binding.fab2);
        animatorSet2.play(animX2).with(animY2).with(animAlpha2);
        animatorSet2.start();

        ObjectAnimator animAlpha3 = ObjectAnimator.ofFloat(binding.fab3, "alpha", 0f, 1f);
        ObjectAnimator animX3 = ObjectAnimator.ofFloat(binding.fab3,"translationX",X2);
        animX3.setDuration(1000);
        ObjectAnimator animY3 = ObjectAnimator.ofFloat(binding.fab3,"translationY",Y2);
        animY3.setDuration(1000);
        AnimatorSet animatorSet3 = new AnimatorSet();
        animatorSet3.setTarget(binding.fab3);
        animatorSet3.play(animX3).with(animY3).with(animAlpha3);
        animatorSet3.start();
    }



//    @Override
//    public boolean onTouch(View view, MotionEvent motionEvent) {
//        int x = (int) motionEvent.getX();
//        int y = (int) motionEvent.getY();
//        switch (motionEvent.getAction()) {
//            case MotionEvent.ACTION_HOVER_ENTER:
//                Toast.makeText(getContext(), "TOUCH ON "+x+" , "+y, Toast.LENGTH_SHORT).show();
//                break;
//            case MotionEvent.ACTION_UP:
//                binding.layoutShare.setVisibility(View.GONE);
//                binding.layoutShare.setClickable(false);
//                break;
//            default:
//                break;
//        }
//        return false;
//    }
}
