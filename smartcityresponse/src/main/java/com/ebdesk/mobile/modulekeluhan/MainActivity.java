package com.ebdesk.mobile.modulekeluhan;

import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ebdesk.mobile.modulekeluhan.view.AspirasiKeluhanFragment;
import com.ebdesk.mobile.modulekeluhan.view.MainKeluhanFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainKeluhanFragment myf = MainKeluhanFragment.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.keluhan_content,  myf).commit();
    }
}
