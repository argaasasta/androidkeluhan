package com.ebdesk.mobile.modulekeluhan.model;

import com.ebdesk.mobile.modulekeluhan.model.model_aspirasi.Aspirasi;
import com.ebdesk.mobile.modulekeluhan.model.model_category.Category;
import com.ebdesk.mobile.modulekeluhan.model.model_info_kota.InfoKota;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by EB-NB21 on 3/14/2017.
 */

public interface ServiceApi {

    public static final int TYPE_GSON = 1;
    public static final int TYPE_STRING = 2;

    @GET("users/{username}/repos")
    Observable<String> publicRepositories(@Path("username") String username);

    @GET("json?sensor=true")
    Observable<String> publicGetStreetName(@Query("latlng") String location);

    @GET("api_smart_city/info_kota/get_city_info?location=3579")
    Observable<InfoKota> publicGetInfoKota(@Query("start") int start, @Query("row") int row);

    //ASPIRASI
    @GET("api_smart_city/info_kota/get_aspiration_category?location=3579")
    Observable<Category> publicGetCategory(@Query("start") int start, @Query("rows") int row);

    @GET("api_smart_city/info_kota/get_aspiration?location=3579")
    Observable<Aspirasi> publicGetAspirasi(@Query("start") int start,@Query("row") int row);

    @GET("api_smart_city/info_kota/get_aspiration?location=3579")
    Observable<Aspirasi> publicGetAspirasiTerkait(@Query("category") String category_id,@Query("start") int start);

    @Multipart
    @POST("/api/aspirasi/savedata")
    Observable<String> postUpdateActivity(@Part("category_id") RequestBody categoryId,
                                          @Part("category_name") RequestBody categoryName,
                                          @Part("desc") RequestBody desc,
                                          @Part("latitude") RequestBody latitude,
                                          @Part("longitude") RequestBody longitude,
                                          @Part("created_by") RequestBody createdBy,
                                          @Part("media_type") RequestBody mediaType,
                                          @Part MultipartBody.Part filePart);

    class Factory {
        public static ServiceApi createServiceGeocode() {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://maps.googleapis.com/maps/api/geocode/")
//                    .addConverterFactory(GsonConverterFactory.createServiceGeocode())
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            return retrofit.create(ServiceApi.class);
        }

        public static ServiceApi createServiceInfoKota() {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://103.43.128.142:8420/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            return retrofit.create(ServiceApi.class);
        }

        public static ServiceApi createServiceAspirasi(int type) {
            Retrofit retrofit = null;
            if (type == TYPE_GSON) {
                retrofit = new Retrofit.Builder()
                        .baseUrl("http://103.43.128.142:8880/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .build();
            } else if (type == TYPE_STRING) {
                retrofit = new Retrofit.Builder()
                        .baseUrl("http://103.43.128.142:8880/")
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .build();
            }
            return retrofit.create(ServiceApi.class);
        }
    }
}
