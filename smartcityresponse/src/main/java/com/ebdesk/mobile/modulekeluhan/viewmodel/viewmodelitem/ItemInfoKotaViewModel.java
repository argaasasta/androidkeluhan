package com.ebdesk.mobile.modulekeluhan.viewmodel.viewmodelitem;

import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.ObservableInt;
import android.net.Uri;
import android.view.View;

import com.ebdesk.mobile.modulekeluhan.model.model_info_kota.ItemInfoKota;
import com.ebdesk.mobile.modulekeluhan.viewmodel.ViewModel;

/**
 * Created by EB-NB21 on 3/10/2017.
 */

public class ItemInfoKotaViewModel extends BaseObservable implements ViewModel{

    private ItemInfoKota itemInfoKota;
    private Context context;
    public ObservableInt imageInfoVisibility;
    public ItemInfoKotaViewModel(Context context, ItemInfoKota itemInfoKota) {
        this.context = context;
        this.itemInfoKota = itemInfoKota;
        this.imageInfoVisibility = new ObservableInt(View.GONE);
        if(!itemInfoKota.getInfoImage().equals("")){
            imageInfoVisibility.set(View.VISIBLE);
        }else{
            imageInfoVisibility.set(View.GONE);

        }
    }
    // Allows recycling ItemRepoViewModels within the recyclerview adapter
    public void setInfoKotaOld(ItemInfoKota itemInfoKota) {
        this.itemInfoKota = itemInfoKota;
        if(!itemInfoKota.getInfoImage().equals("")){
            imageInfoVisibility.set(View.VISIBLE);
        }else{
            imageInfoVisibility.set(View.GONE);

        }
        notifyChange();
    }

    public String getId() {
        return itemInfoKota.getId()+"";
    }

    public String getCreator() {
        return itemInfoKota.getName();
    }

    public String getCreatorPicture() {
        return  "http://103.43.128.142:8420"+itemInfoKota.getMediaImage();
    }

    public String getDesc() {
        return  itemInfoKota.getDesc();
    }

    public String getDescPic() {
        return  itemInfoKota.getInfoImage();
    }

    public String getDate() {
        return  itemInfoKota.getPubDate();
    }

    public void onDetailClick(View view){
        if(itemInfoKota.getLink()!=null) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(itemInfoKota.getLink()));
            context.startActivity(browserIntent);
        }
    }
    @Override
    public void destroy() {

    }
}
