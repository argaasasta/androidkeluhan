package com.ebdesk.mobile.modulekeluhan.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebdesk.mobile.modulekeluhan.R;
import com.ebdesk.mobile.modulekeluhan.databinding.FragmentMainKeluhanBinding;
import com.ebdesk.mobile.modulekeluhan.viewmodel.MainKeluhanViewModel;

public class MainKeluhanFragment extends Fragment {

    FragmentMainKeluhanBinding binding;
    MainKeluhanViewModel mainKeluhanViewModel;

    public static MainKeluhanFragment newInstance() {
        MainKeluhanFragment fragment = new MainKeluhanFragment();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main_keluhan, container, false);
        View view = binding.getRoot();
        mainKeluhanViewModel = new MainKeluhanViewModel(getContext(),getFragmentManager());
        binding.tabs.setupWithViewPager(binding.viewPager);
        binding.setViewModel(mainKeluhanViewModel);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        //save RecyclerView state
    }



}
