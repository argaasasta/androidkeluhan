package com.ebdesk.mobile.modulekeluhan.viewmodel;

/**
 * Interface that every ViewModel must implement
 */
public interface ViewModel {

    void destroy();
}
