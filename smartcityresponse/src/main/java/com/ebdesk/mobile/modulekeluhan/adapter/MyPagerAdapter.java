package com.ebdesk.mobile.modulekeluhan.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ebdesk.mobile.modulekeluhan.view.AspirasiKeluhanFragment;
import com.ebdesk.mobile.modulekeluhan.view.InfoKotaFragment;

/**
 * Created by EB-NB21 on 3/10/2017.
 */


public class MyPagerAdapter extends FragmentStatePagerAdapter {

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int pos) {
        switch(pos) {

            case 0: return InfoKotaFragment.newInstance();
            case 1: return AspirasiKeluhanFragment.newInstance("NORMAL");
            default: return InfoKotaFragment.newInstance();
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "Info Kota";
        }
        else if (position == 1)
        {
            title = "Aspirasi";
        }
        return title;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
