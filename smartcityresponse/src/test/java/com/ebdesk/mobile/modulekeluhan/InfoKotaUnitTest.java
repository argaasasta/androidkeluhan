package com.ebdesk.mobile.modulekeluhan;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * InfoKota local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class InfoKotaUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }
}