package com.ebdesk.mobile.smartresponse.view;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.ebdesk.mobile.modulekeluhan.view.MainKeluhanFragment;
import com.ebdesk.mobile.smartresponse.R;
import com.ebdesk.mobile.smartresponse.util.ActivityUtils;

public class MainSmartResponseActivity extends AppCompatActivity {

    Toolbar mToolbar;
    DrawerLayout mDrawerLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_smart_response);
        mToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout_main);
        setSupportActionBar(mToolbar);
        final ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, 0,0){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
//                binding.appBarMain.toolbar.toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

//        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawerToggle.setHomeAsUpIndicator(android.R.drawable.alert_dark_frame);
        drawerToggle.setDrawerIndicatorEnabled(false);
        drawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mDrawerLayout.isDrawerVisible(GravityCompat.START)){
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                }else{
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });

//        mDrawerLayout.addDrawerListener(drawerToggle);
//        mDrawerLayout.post(new Runnable() {
//            @Override
//            public void run() {
//                drawerToggle.syncState();
//            }
//        });

        ActivityUtils.replaceFragmentToActivity(this.getSupportFragmentManager(), MainKeluhanFragment.newInstance(), R.id.main_fragment_response);
    }
}
