package com.isas.module.getmedia;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;

import life.knowledge4.videotrimmer.K4LVideoTrimmer;
import life.knowledge4.videotrimmer.interfaces.OnTrimVideoListener;


public class VideoTrimmer extends AppCompatActivity implements OnTrimVideoListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_trimmer);

        String path = getIntent().getStringExtra("PATH_VIDEO");
        System.out.println("PATH TRIMMER DITERIMA "+path);
        K4LVideoTrimmer videoTrimmer = ((K4LVideoTrimmer) findViewById(R.id.timeLine));
        if (videoTrimmer != null) {
            videoTrimmer.setMaxDuration(30);
            videoTrimmer.setOnTrimVideoListener(this);
            //mVideoTrimmer.setDestinationPath("/storage/emulated/0/DCIM/CameraCustom/");
            videoTrimmer.setVideoURI(Uri.parse(path));
        }

        Environment.getExternalStorageDirectory();
    }


    @Override
    public void onTrimStarted() {

    }

    @Override
    public void getResult(Uri uri) {
        System.out.println("PATH TRIMMER HASIL: "+uri.getPath());
        Intent resultIntent = new Intent();
        resultIntent.putExtra("PATH_TRIMMER_HASIL", uri.getPath());
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public void cancelAction() {
        finish();
    }

    @Override
    public void onError(String message) {

    }
}
