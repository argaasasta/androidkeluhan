package com.isas.module.getmedia.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.isas.module.getmedia.R;
import com.isas.module.getmedia.model.Image;

import java.util.ArrayList;

/**
 * Created by Darshan on 4/18/2015.
 */
public class CustomImageSelectAdapter extends RecyclerView.Adapter<CustomImageSelectAdapter.ViewHolder> {
    ArrayList<Image> images;
    Context context;
    DataListener dataListener;

    public CustomImageSelectAdapter(Context context, ArrayList<Image> images, DataListener dataListener) {
        this.context = context;
        this.images = images;
        this.dataListener = dataListener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_image_select, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Glide.with(context)
                .load(images.get(position).path)
                .override(100, 100)
                .placeholder(R.drawable.image_placeholder).into(holder.imageViewImageSelected);
        holder.imageViewImageSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dataListener.onClickImage(position);
            }
        });

        if (images.get(position).isSelected) {
            holder.vAlpha.setAlpha(0.5f);
            ((FrameLayout) holder.mView).setForeground(context.getResources().getDrawable(R.drawable.ic_done_white));

        } else {
            holder.vAlpha.setAlpha(0.0f);
            ((FrameLayout) holder.mView).setForeground(null);
        }

    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public interface DataListener {
        void onClickImage(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public ImageView imageViewImageSelected;
        public View vAlpha;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            imageViewImageSelected = (ImageView) view.findViewById(R.id.image_view_image_select);
            vAlpha = (View) view.findViewById(R.id.view_alpha);

        }

    }
}
