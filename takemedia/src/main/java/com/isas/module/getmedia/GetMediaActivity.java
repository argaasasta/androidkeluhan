package com.isas.module.getmedia;

import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.ExifInterface;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.isas.module.getmedia.adapter.CustomImageSelectAdapter;
import com.isas.module.getmedia.helper.HelperActivity;
import com.isas.module.getmedia.helper.UtilMethod;
import com.isas.module.getmedia.helpers.Constants;
import com.isas.module.getmedia.helpers.SingleShotLocationProvider;
import com.isas.module.getmedia.model.Image;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

public class GetMediaActivity extends HelperActivity implements CustomImageSelectAdapter.DataListener, Camera.ShutterCallback, Camera.PictureCallback, SingleShotLocationProvider.LocationCallback {
    public final static int GET_TRIMMER_PATH = 8;


    private ArrayList<Image> images;
    private final String VIDEO_PATH_NAME = Environment.getExternalStorageDirectory() + "/tryvideo";
    private RecyclerView mRecyclerView;
    private CustomImageSelectAdapter adapter;
    private boolean takePictureAct;
    private ActionBar actionBar;
    private RelativeLayout rlTakeMediaLoading;

    private ActionMode actionMode;
    private int countSelected;

    private ContentObserver observer;
    private Handler handler;
    private Thread thread;
    public Uri urlVideo;
    Camera mCamera;
    private MyCameraSurfaceView myCameraSurfaceView;

    SurfaceView mPreview;
    //    ImageView imgResult;
    double latitudeSavePhoto = 0.0;
    double longitudeSavePhoto = 0.0;
    Button btnSnap;

    MediaRecorder mediaRecorder;
    boolean recording;

    private final String[] projection = new String[]{MediaStore.Images.Media._ID, MediaStore.Images.Media.DISPLAY_NAME, MediaStore.Images.Media.DATA};
    private final String[] projection2 = {
            MediaStore.Files.FileColumns._ID,
            MediaStore.Files.FileColumns.DISPLAY_NAME,
            MediaStore.Files.FileColumns.DATA,
            MediaStore.Files.FileColumns.DATE_ADDED,
            MediaStore.Files.FileColumns.MEDIA_TYPE,
            MediaStore.Files.FileColumns.MIME_TYPE,
            MediaStore.Files.FileColumns.TITLE
    };

    void onProgressTakeMedia(boolean show){
        if(show){
            rlTakeMediaLoading.setVisibility(View.VISIBLE);
        }else {
            rlTakeMediaLoading.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_media);
        setView(findViewById(R.id.layout_image_select));
        Constants.limit = 1;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        rlTakeMediaLoading= (RelativeLayout) findViewById(R.id.layout_take_media_loading);
        takePictureAct = false;

        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

            actionBar.setDisplayShowTitleEnabled(true);
//            actionBar.setTitle(R.string.image_view);
        }
        SingleShotLocationProvider.requestSingleUpdate(this, this);
        Intent intent = getIntent();
        if (intent == null) {
            finish();
        }

//        mPreview = (SurfaceView) findViewById(R.id.surfaceview);
//        mPreview.getHolder().addCallback(this);
//        mPreview.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        mCamera = Camera.open();
        Camera.Parameters params = mCamera.getParameters();

        List<Camera.Size> sizes = params.getSupportedPictureSizes();
        Camera.Size size = sizes.get(0);
//Camera.Size size1 = sizes.get(0);
        for (int i = 0; i < sizes.size(); i++) {
            if (sizes.get(i).width > size.width)
                size = sizes.get(i);


        }


        params.setPictureSize(size.width, size.height);
        params.setExposureCompensation(0);
        params.setPictureFormat(ImageFormat.JPEG);
        params.setJpegQuality(70);


        mCamera.setParameters(params);

        myCameraSurfaceView = new MyCameraSurfaceView(this, mCamera, this);
        FrameLayout myCameraPreview = (FrameLayout) findViewById(R.id.videoview);
        myCameraPreview.addView(myCameraSurfaceView);

        recording = false;

        mediaRecorder = new MediaRecorder();

        btnSnap = (Button) findViewById(R.id.btn_shutter);
        btnSnap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSnapClick();
            }
        });
        btnSnap.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mCamera.release();
                prepareVideoRecorder();
                mediaRecorder.start();
                recording = true;
                return true;
            }
        });

        btnSnap.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (recording) {
                        onProgressTakeMedia(true);
                        ContentValues values = new ContentValues();
                        values.put(MediaStore.Video.Media.TITLE, "Title1");
                        values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
                        values.put(MediaStore.Video.Media.DATA, urlVideo.getPath());
                        Uri uri = getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);
                        try {
                            System.out.println("PAS SAVE VIDEO "+getRealPathFromURI(getApplicationContext(),uri));
//                            ExifInterface exif = new ExifInterface(getRealPathFromURI(getApplicationContext(), uri));
//                            exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, GPS.convert(latitudeSavePhoto));
//                            exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, GPS.latitudeRef(latitudeSavePhoto));
//                            exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, GPS.convert(longitudeSavePhoto));
//                            exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, GPS.longitudeRef(longitudeSavePhoto));
//                            exif.saveAttributes();
                            mediaRecorder.stop();
                            mediaRecorder.reset();
                            mediaRecorder.release();
                            sendIntentVideo(getRealPathFromURI(getApplicationContext(),uri));
                            finish();
                        } catch (Exception e) {
                            System.out.println("ERROR MASUKIN EXIF KE VIDEO");
                            e.printStackTrace();

                            finish();
                        }
                    }
                }
                return false;
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(layoutManager);

//        imgResult = (ImageView) findViewById(R.id.image_result);


        setTitle("");


    }

    @Override
    protected void onStart() {
        super.onStart();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case Constants.PERMISSION_GRANTED: {
                        loadImages();
                        break;
                    }

                    case Constants.FETCH_STARTED: {
                        mRecyclerView.setVisibility(View.INVISIBLE);
                        break;
                    }

                    case Constants.FETCH_COMPLETED: {
                        /*
                        If adapter is null, this implies that the loaded images will be shown
                        for the first time, hence send FETCH_COMPLETED message.
                        However, if adapter has been initialised, this thread was run either
                        due to the activity being restarted or content being changed.
                         */
                        if (adapter == null) {
                            adapter = new CustomImageSelectAdapter(getApplicationContext(), images, GetMediaActivity.this);
                            mRecyclerView.setAdapter(adapter);

                            mRecyclerView.setVisibility(View.VISIBLE);
//                            orientationBasedUI(getResources().getConfiguration().orientation);

                        } else {
                            adapter.notifyDataSetChanged();
                            /*
                            Some selected images may have been deleted
                            hence update action mode title
                             */
                            if (actionMode != null) {
                                countSelected = 0;
                                actionMode.setTitle(countSelected + " " + getString(R.string.selected));
                            }
                        }
//                        if (takePictureAct) {
//                            toggleSelection(0);
//                            sendIntent();
//                        }
                        break;
                    }

                    case Constants.ERROR: {
                        break;
                    }

                    default: {
                        super.handleMessage(msg);
                    }
                }
            }
        };
        observer = new ContentObserver(handler) {
            @Override
            public void onChange(boolean selfChange) {
                System.out.println();
                loadImages();
            }
        };
        getContentResolver().registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, false, observer);

        checkPermission();
    }

    @Override
    protected void onStop() {
        super.onStop();

        stopThread();

        getContentResolver().unregisterContentObserver(observer);
        observer = null;

        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(null);
        }
        images = null;
        if (adapter != null) {
//            adapter.releaseResources();
        }
        mCamera.release();
        Log.d("CAMERA", "Destroy");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
//        orientationBasedUI(newConfig.orientation);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
                return true;
            }

            default: {
                return false;
            }
        }
    }

    private ActionMode.Callback callback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater menuInflater = mode.getMenuInflater();
            menuInflater.inflate(R.menu.menu_contextual_action_bar, menu);

            actionMode = mode;
            countSelected = 0;

            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            int i = item.getItemId();
            if (i == R.id.menu_item_add_image) {
                sendIntent();
                return true;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            if (countSelected > 0) {
                deselectAll();
            }
            actionMode = null;
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case GET_TRIMMER_PATH:
                    if (resultCode == RESULT_OK && data != null) {
                        String newPath = data.getStringExtra("PATH_TRIMMER_HASIL");
                        sendIntentVideo(newPath);
                    }
                default:
                    break;
            }
        }else{
            System.out.println("MASUK ELSE KALO DI CANCEL");
            deselectAll();
        }

    }

    private void toggleSelection(int position) {
        if (!images.get(position).isSelected && countSelected >= Constants.limit) {
            Toast.makeText(
                    getApplicationContext(),
                    String.format(getString(R.string.limit_exceeded), Constants.limit),
                    Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        images.get(position).isSelected = !images.get(position).isSelected;
        if (images.get(position).isSelected) {
            countSelected++;
        } else {
            countSelected--;
        }
        adapter.notifyDataSetChanged();
    }

    private void deselectAll() {
        for (int i = 0, l = images.size(); i < l; i++) {
            images.get(i).isSelected = false;
        }
        countSelected = 0;
        adapter.notifyDataSetChanged();
    }

    private ArrayList<Image> getSelected() {
        ArrayList<Image> selectedImages = new ArrayList<>();
        for (int i = 0, l = images.size(); i < l; i++) {
            if (images.get(i).isSelected) {
                selectedImages.add(images.get(i));
            }
        }
        return selectedImages;
    }

    private void sendIntent() {

        ArrayList<Image> selectedFile = getSelected();
        if(UtilMethod.getMimeType(selectedFile.get(0).path).equals("video/mp4")){
            trimVideoAction(selectedFile.get(0).path);
        }else {
            Intent intent = new Intent();
            intent.putParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES, getSelected());
            setResult(RESULT_OK, intent);
            finish();
        }

    }
    private void sendIntentTakePicture(String path) {
        ArrayList<Image> selectedImages = new ArrayList<>();
        selectedImages.add(new Image(10000778, "IMAGEKELUHAN", path, true));

        Intent intent = new Intent();
        intent.putParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES, selectedImages);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void sendIntentVideo(String path) {
        ArrayList<Image> selectedImages = new ArrayList<>();
        selectedImages.add(new Image(10000777, "VIDEOKELUHAN", path, true));

        Intent intent = new Intent();
        intent.putParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES, selectedImages);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void trimVideoAction(String dataPath){
        Intent intent = new Intent(this, VideoTrimmer.class);
        String data = dataPath;
        String path = "";
        if (data.contains("file://") && data.contains(".mp4")) {
            path = data.substring(7, data.length());
            intent.putExtra("PATH_VIDEO", path);
        } else {
            Uri vid = Uri.parse(data);
            intent.putExtra("PATH_VIDEO", dataPath);
        }
        startActivityForResult(intent, GET_TRIMMER_PATH);
    }

    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private void loadImages() {
        startThread(new ImageLoaderRunnable());
    }

    @Override
    public void onClickImage(int position) {
//        if (actionMode == null) {
//            actionMode = GetMediaActivity.this.startActionMode(callback);
//        }
        toggleSelection(position);
        sendIntent();
    }

    @Override
    public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
        latitudeSavePhoto = location.latitude;
        longitudeSavePhoto = location.longitude;
        System.out.println("DAPET GA SIH SINGLE SHOOT " + latitudeSavePhoto);
    }

    private class ImageLoaderRunnable implements Runnable {
        @Override
        public void run() {
            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            /*
            If the adapter is null, this is first time this activity's view is
            being shown, hence send FETCH_STARTED message to show progress bar
            while images are loaded from phone
             */
            if (adapter == null) {
                sendMessage(Constants.FETCH_STARTED);
            }

            File file;
            HashSet<Long> selectedImages = new HashSet<>();
            if (images != null) {
                Image image;
                for (int i = 0, l = images.size(); i < l; i++) {
                    image = images.get(i);
                    file = new File(image.path);
                    if (file.exists() && image.isSelected) {
                        selectedImages.add(image.id);
                    }
                }
            }
            String selection = MediaStore.Files.FileColumns.MEDIA_TYPE + " = "
                    + MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
                    + " OR "
                    + MediaStore.Files.FileColumns.MEDIA_TYPE + " = "
                    + MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;
//            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            Cursor cursor = getContentResolver().query(MediaStore.Files.getContentUri("external"), projection2,
                    selection, null, MediaStore.Images.Media.DATE_ADDED);
            if (cursor == null) {
                sendMessage(Constants.ERROR);
                return;
            }

            /*
            In case this runnable is executed to onChange calling loadImages,
            using countSelected variable can result in a race condition. To avoid that,
            tempCountSelected keeps track of number of selected images. On handling
            FETCH_COMPLETED message, countSelected is assigned value of tempCountSelected.
             */
            int tempCountSelected = 0;
            ArrayList<Image> temp = new ArrayList<>(cursor.getCount());
            if (cursor.moveToLast()) {
                do {
                    if (Thread.interrupted()) {
                        return;
                    }

                    long id = cursor.getLong(cursor.getColumnIndex(projection[0]));
                    String name = cursor.getString(cursor.getColumnIndex(projection[1]));
                    String path = cursor.getString(cursor.getColumnIndex(projection[2]));
                    boolean isSelected = selectedImages.contains(id);
                    if (isSelected) {
                        tempCountSelected++;
                    }

                    file = new File(path);

                    if (file.exists()) {
                        try {
                            System.out.println("APA SIH MIME TYPENYA "+UtilMethod.getMimeType(path));
                            if (UtilMethod.getMimeType(path).equals("video/mp4")) {
                                temp.add(new Image(id, name, path, isSelected));
                            } else {
                                ExifInterface exif = new ExifInterface(path);
                                String LATITUDE = exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE);
                                String LATITUDE_REF = exif.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF);
                                String LONGITUDE = exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE);
                                String LONGITUDE_REF = exif.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF);
                                System.out.println("ADA IMAGE YG ADA EXIFNYA " + path + " : " + LATITUDE);
                                Double Latitude, Longitude;
                                if ((LATITUDE != null) && (LATITUDE_REF != null) && (LONGITUDE != null) && (LONGITUDE_REF != null)) {

                                    if (LATITUDE_REF.equals("N")) {
                                        Latitude = convertToDegree(LATITUDE);
                                    } else {
                                        Latitude = 0 - convertToDegree(LATITUDE);
                                    }

                                    if (LONGITUDE_REF.equals("E")) {
                                        Longitude = convertToDegree(LONGITUDE);
                                    } else {
                                        Longitude = 0 - convertToDegree(LONGITUDE);
                                    }
                                    System.out.println("BISA DOUBLE GA DAPET URL GBRNYA " + path + " LATITUDE " + Latitude + " LONGITUDE " + Longitude);
                                    temp.add(new Image(id, name, path, isSelected));
                                }
                            }
                        } catch (Exception e) {
                            System.out.println("tryimage ada yg null");
                            e.printStackTrace();
                        }
                    }

                } while (cursor.moveToPrevious());
            }
            cursor.close();//http://www.arvlozyax.com/2016/11/download-anime-lets-go-tamiya-dub-dan-sub-indo.html

            if (images == null) {
                images = new ArrayList<>();
            }
            images.clear();
            images.addAll(temp);

            sendMessage(Constants.FETCH_COMPLETED, tempCountSelected);
        }
    }

    private Double convertToDegree(String stringDMS) {
        Double result = null;
        String[] DMS = stringDMS.split(",", 3);

        String[] stringD = DMS[0].split("/", 2);
        Double D0 = new Double(stringD[0]);
        Double D1 = new Double(stringD[1]);
        Double FloatD = D0 / D1;

        String[] stringM = DMS[1].split("/", 2);
        Double M0 = new Double(stringM[0]);
        Double M1 = new Double(stringM[1]);
        Double FloatM = M0 / M1;

        String[] stringS = DMS[2].split("/", 2);
        Double S0 = new Double(stringS[0]);
        Double S1 = new Double(stringS[1]);
        Double FloatS = S0 / S1;

        result = new Double(FloatD + (FloatM / 60) + (FloatS / 3600));

        return result;


    }

    private void startThread(Runnable runnable) {
        stopThread();
        thread = new Thread(runnable);
        thread.start();
    }

    private void stopThread() {
        if (thread == null || !thread.isAlive()) {
            return;
        }

        thread.interrupt();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void sendMessage(int what) {
        sendMessage(what, 0);
    }

    private void sendMessage(int what, int arg1) {
        if (handler == null) {
            return;
        }

        Message message = handler.obtainMessage();
        message.what = what;
        message.arg1 = arg1;
        message.sendToTarget();
    }

    @Override
    protected void permissionGranted() {
        sendMessage(Constants.PERMISSION_GRANTED);
    }

    @Override
    protected void hideViews() {
        mRecyclerView.setVisibility(View.INVISIBLE);
    }


    //CAMERA ACTIVITY
    @Override
    public void onPause() {
        super.onPause();
        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onCancelClick(View v) {
        finish();
    }

    public void onSnapClick() {
        mCamera.takePicture(this, null, null, this);
    }

    @Override
    public void onShutter() {
//        Toast.makeText(this, "Click!", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
//        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//        newBitmapRotate.compress(Bitmap.CompressFormat.PNG, 100, stream);
//        byte[] byteArray = stream.toByteArray();

        //Step 1. Create file for storing image data on SDCard
        System.out.println("YG LAMA DI SINI BUKAN SIH");
        onProgressTakeMedia(true);
        new SavePictureOperation(data).execute();
//        savePhotoAction(data);

        camera.startPreview();
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    private class SavePictureOperation extends AsyncTask<String, Void, String> {
        byte[] arrayBytePhoto;
        Uri url = null;
        public SavePictureOperation(byte[] arrayBytePhoto) {
            this.arrayBytePhoto=arrayBytePhoto;
        }

        @Override
        protected String doInBackground(String... params) {
            savePhotoAction(arrayBytePhoto);
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            sendIntentTakePicture(getRealPathFromURI(getApplicationContext(), url));
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}

        public void savePhotoAction(byte[] arrayBytePhoto) {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "ImageKeluhan");
            values.put(MediaStore.Images.Media.DISPLAY_NAME, "Keluhan - " + System.currentTimeMillis());
            values.put(MediaStore.Images.Media.DESCRIPTION, "Keluhan image");
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
            // Add the date meta data to ensure the image is added at the front of the gallery
            values.put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis());
            values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());


            Bitmap cropped = BitmapFactory.decodeByteArray(arrayBytePhoto, 0, arrayBytePhoto.length);
            int orientation = getOrientation(arrayBytePhoto);
            Bitmap newBitmapRotate = null;
            System.out.println("ORIENTASINYA APA " + orientation);
            switch (orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    newBitmapRotate = rotateImage(cropped, 90);
                    Log.d("orientation 90", "masukBitMapCamera");
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    newBitmapRotate = rotateImage(cropped, 180);
                    Log.d("orientation 180", "masukBitMapCamera");
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    newBitmapRotate = rotateImage(cropped, 270);
                    Log.d("orientation 270", "masukBitMapCamera");
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                    Log.d("orientation normal", "masukBitMapCamera");
                    newBitmapRotate = rotateImage(cropped, 90);
                    break;
                default:
                    System.out.println("BERAPA SIH BUILDNYA " + Build.VERSION.SDK_INT);
                    if (Build.VERSION.SDK_INT > 19)
                        newBitmapRotate = rotateImage(cropped, 90);
                    else
                        newBitmapRotate = cropped;
                    break;
            }
            try {


                url = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                OutputStream imageOut = getContentResolver().openOutputStream(url);
                try {
                    newBitmapRotate.compress(Bitmap.CompressFormat.JPEG, 100, imageOut);
                } finally {
                    imageOut.close();
                }
                System.out.println("ISI PATH D SAVE " + getRealPathFromURI(getApplicationContext(), url));
                System.out.println("ISI LOCATION PATH SAVE "+latitudeSavePhoto+","+longitudeSavePhoto);
                ExifInterface exif = new ExifInterface(getRealPathFromURI(getApplicationContext(), url));
                exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, GPS.convert(latitudeSavePhoto));
                exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, GPS.latitudeRef(latitudeSavePhoto));
                exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, GPS.convert(longitudeSavePhoto));
                exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, GPS.longitudeRef(longitudeSavePhoto));
                exif.saveAttributes();
            } catch (Exception e) {
                Log.e("PictureDemo", "Exception in photoCallback", e);
            }

//        takePictureAct = true;
        }
    }


    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    public static int getOrientation(byte[] jpeg) {
        if (jpeg == null) {
            return 0;
        }

        int offset = 0;
        int length = 0;

        // ISO/IEC 10918-1:1993(E)
        while (offset + 3 < jpeg.length && (jpeg[offset++] & 0xFF) == 0xFF) {
            int marker = jpeg[offset] & 0xFF;

            // Check if the marker is a padding.
            if (marker == 0xFF) {
                continue;
            }
            offset++;

            // Check if the marker is SOI or TEM.
            if (marker == 0xD8 || marker == 0x01) {
                continue;
            }
            // Check if the marker is EOI or SOS.
            if (marker == 0xD9 || marker == 0xDA) {
                break;
            }

            // Get the length and check if it is reasonable.
            length = pack(jpeg, offset, 2, false);
            if (length < 2 || offset + length > jpeg.length) {
                Log.e("ORIENTATION", "Invalid length");
                return 0;
            }

            // Break if the marker is EXIF in APP1.
            if (marker == 0xE1 && length >= 8 &&
                    pack(jpeg, offset + 2, 4, false) == 0x45786966 &&
                    pack(jpeg, offset + 6, 2, false) == 0) {
                offset += 8;
                length -= 8;
                break;
            }

            // Skip other markers.
            offset += length;
            length = 0;
        }

        if (length > 8) {
            // Identify the byte order.
            int tag = pack(jpeg, offset, 4, false);
            if (tag != 0x49492A00 && tag != 0x4D4D002A) {
                Log.e("ORIENTATION", "Invalid byte order");
                return 0;
            }
            boolean littleEndian = (tag == 0x49492A00);

            // Get the offset and check if it is reasonable.
            int count = pack(jpeg, offset + 4, 4, littleEndian) + 2;
            if (count < 10 || count > length) {
                Log.e("ORIENTATION", "Invalid offset");
                return 0;
            }
            offset += count;
            length -= count;

            // Get the count and go through all the elements.
            count = pack(jpeg, offset - 2, 2, littleEndian);
            while (count-- > 0 && length >= 12) {
                // Get the tag and check if it is orientation.
                tag = pack(jpeg, offset, 2, littleEndian);
                if (tag == 0x0112) {
                    // We do not really care about type and count, do we?
                    int orientation = pack(jpeg, offset + 8, 2, littleEndian);
                    switch (orientation) {
                        case 1:
                            return ExifInterface.ORIENTATION_NORMAL;
                        case 3:
                            return ExifInterface.ORIENTATION_ROTATE_180;
                        case 6:
                            return ExifInterface.ORIENTATION_ROTATE_90;
                        case 8:
                            return ExifInterface.ORIENTATION_ROTATE_270;
                    }
                    Log.i("ORIENTATION", "Unsupported orientation");
                    return 0;
                }
                offset += 12;
                length -= 12;
            }
        }

        Log.i("ORIENTATION", "Orientation not found");
        return 0;
    }

    private static int pack(byte[] bytes, int offset, int length,
                            boolean littleEndian) {
        int step = 1;
        if (littleEndian) {
            offset += length - 1;
            step = -1;
        }

        int value = 0;
        while (length-- > 0) {
            value = (value << 8) | (bytes[offset] & 0xFF);
            offset += step;
        }
        return value;
    }

    //TAKE VIDEO ACTIVITY
    private boolean prepareVideoRecorder() {

        mCamera = getCameraInstance();
        setCameraDisplayOrientation(this, 0, mCamera);

        mediaRecorder = new MediaRecorder();

        // Step 1: Unlock and set camera to MediaRecorder
        mCamera.unlock();
        mediaRecorder.setCamera(mCamera);

        // Step 2: Set sources
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
        mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));

        // Step 4: Set output file
        System.out.println("BISA GA DAPET PATH VIDEO " + getOutputMediaFile(MEDIA_TYPE_VIDEO).toString());
        urlVideo = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);
        String pathNewVideo = "/sdcard/keluhanVideo" + System.currentTimeMillis() + ".mp4";
//        urlVideo = Uri.fromFile(new File(pathNewVideo));
        mediaRecorder.setOutputFile(urlVideo.getPath());
        mediaRecorder.setMaxDuration(30000);

        // Step 5: Set the preview output
        mediaRecorder.setPreviewDisplay(myCameraSurfaceView.getHolder().getSurface());
        mediaRecorder.setOrientationHint(RecordVideoActivity.orientation);

        // Step 6: Prepare configured MediaRecorder
        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    private static Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * Create a File for saving an image or video
     */
    private static File getOutputMediaFile(int type) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MOVIES), "KeluhanVideo");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    private Camera getCameraInstance() {
        // TODO Auto-generated method stub
        Camera c = null;
        try {
            c = Camera.open(0); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.reset();   // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = new MediaRecorder();
            mCamera.lock();           // lock camera for later use
        }
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }

    public class MyCameraSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

        private SurfaceHolder mHolder;
        private Camera mCamera;
        private Activity mActivity;

        public MyCameraSurfaceView(Context context, Camera camera, Activity activity) {
            super(context);

            mCamera = camera;


            mActivity = activity;
            // Install a SurfaceHolder.Callback so we get notified when the
            // underlying surface is created and destroyed.
            mHolder = getHolder();
            mHolder.addCallback(this);
            // deprecated setting, but required on Android versions prior to 3.0
            mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            try {
                setCameraDisplayOrientation(mActivity, 0, mCamera);
                previewCamera();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void previewCamera() {
            try {
                mCamera.setPreviewDisplay(mHolder);
                mCamera.startPreview();
            } catch (Exception e) {
                //Log.d(APP_CLASS, "Cannot start preview", e);
            }
        }


        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            // TODO Auto-generated method stub
            // The Surface has been created, now tell the camera where to draw the preview.
            try {
                mCamera.setPreviewDisplay(holder);
                mCamera.startPreview();
            } catch (IOException e) {
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            // TODO Auto-generated method stub

        }


    }

    public static void setCameraDisplayOrientation(Activity activity,
                                                   int cameraId, android.hardware.Camera camera) {

        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();

        android.hardware.Camera.getCameraInfo(cameraId, info);

        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        RecordVideoActivity.orientation = result;
        camera.setDisplayOrientation(result);
    }
}
